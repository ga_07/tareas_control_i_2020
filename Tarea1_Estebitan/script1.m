% Sistemas de Control
% Script - Tarea #1.
% Esteban Rodr�guez Quintana  B66076
clear;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%% EJERCICIO 1 %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Declaraci�n de constantes:
simTime = 7000*2;
A = 6;
g = 9.81;
X_vs = [0.4,0.5,0.6];
K_vs = 0.001;
p = 785;

% simulaci�n del diagrama en Simulink:
SistemaRealTanque;
fig0 = get_param('SistemaRealTanque','Handle');
saveas(fig0,'figuras/SistemaRealTanque_image_F.svg');

% Para Curva Est�tica:
Qe = linspace(0,0.12);

H1 = (1/(p*g*(K_vs*X_vs(1))^2))*(Qe).^2;
H2 = (1/(p*g*(K_vs*X_vs(2))^2))*(Qe).^2;
H3 = (1/(p*g*(K_vs*X_vs(3))^2))*(Qe).^2;

figure;
plot(Qe,H1,'--r');
hold on;
plot(Qe,H2,'b');
hold on;
plot(Qe,H3,'--g');
legend('$X_{vs} = 0.4$','$X_{vs} = 0.5$','$X_{vs} = 0.6$','Interpreter','latex','location','northwest')
title 'Curva est�tica del proceso para posibles valores de apertura X_{vs}';
ylabel ('$H$ $(m)$','Interpreter','latex');
xlabel ('$Qe$ $(\frac{m^3}{s})$','Interpreter','latex');
ylim([0 3.5]);
grid on;
saveas(gcf,'figuras/curva_estatica','svg')


%Para diagramas de bloques del punto e):
%   Declaraci�n de constantes:
ref = 2.5*0.75;   % Corresponde con el 75% del ambito de
                  % medici�n de 0 a 3.3 m, para tener la
                  % referencia en porcentaje para el calculo
                  % del error.
H_0 = 2.5;
X_vs0 = X_vs(1);
K_vc = 0.1;
K_1 = (2/(X_vs0*K_vs))*sqrt(H_0/(p*g));
K_2 = -2*H_0/X_vs0;
K_t = 1/3.3;
T = (2*A/(X_vs0*K_vs))*sqrt(H_0/(p*g));
K = K_vc*K_1*K_t;
K_d = (K_2*K_t)/K;


% Simulink
diagramas_e_a;
fig1 = get_param('diagramas_e_a','Handle');
saveas(fig1,'figuras/diagramas_e_a_image_F.svg');

diagramas_e_b;
fig2 = get_param('diagramas_e_b','Handle');
saveas(fig2,'figuras/diagramas_e_b_image_F.svg');

% Para resuesta de sistema real y linealizado
f;
sim('f');

fig3 = get_param('f','Handle');
saveas(fig1,'figuras/f.svg');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%% EJERCICIO 2 %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Declaraci�n de constantes:
simTime2 = 40;
Kp = 6;

% Simulaci�n y diagrama de bloques:
diagrama_ejercicio2;
sim('diagrama_ejercicio2');
fig4 = get_param('diagrama_ejercicio2','Handle');
saveas(fig4,'figuras/diagrama_ejercicio2.svg');

% Variables para gr�ficar
t = logsout{1}.Values.Time;
u = logsout{1}.Values.Data;
y = logsout{2}.Values.Data;
d = logsout{3}.Values.Data;
r = logsout{4}.Values.Data;

figure;
plot(t,r,'r');
hold on;
plot(t,y,'b');
hold on;
plot(t,d,'g');
legend('$r(s)$','$y(s)$','$d(s)$','Interpreter','latex')
title ('Salida del sistema en funci�n de las dos entradas');
xlabel ('$Tiempo (s)$','Interpreter','latex');
grid on;

saveas(gcf,'figuras/2a','svg')

figure;
plot(t,r,'r');
hold on;
plot(t,u,'b');
hold on;
plot(t,d,'g');
legend('$r(s)$','$y(s)$','$d(s)$','Interpreter','latex')
title ('Se�al de control en funci�n de las dos entradas');
xlabel ('$Tiempo (s)$','Interpreter','latex');
grid on;

saveas(gcf,'figuras/2b','svg');






