% Gabriel Jim�nez Amador - B73895 - Grupo 03

clear;clc;

p = 3;
q = 8;
m = 9;
n = 5;


a = 0.1 * max([p q m n]);
b = 0.1 * min([p q m n]) + 1;

s = tf('s');

P1 = (b*(s+2))/((s + 1 + a*1i) * (s + 1 - a*1i));
P2 = b/(s^2 - a);

%% 1.1
%Kp = 3.6

Kp = 3.6;
C = Kp;

% FTLA
L = P1*C;

figure('Name','LGR del sistema de control 1.1');
rlocus(L)

figure('Name','Respuesta del sistema de control 1.1');
step(feedback(L, 1))

%% 1.2
%Kp = 1.54
%Ti = 0.5
%Td = 1.10497

Kp = 1.54;
C = Kp*(1 + 1/(200/181*s) + 0.5*s);

% FTLA
L = minreal(P1*C);

figure('Name','LGR del sistema de control 1.2');
rlocus(L)

figure('Name','Respuesta del sistema de control 1.2');
step(feedback(L, 1))


%% 2.1
%Kp = 3.77
%Td = 0.408

% Primer intento
Kp = 1;
C = Kp;

% FTLA
L = P2*C;

figure('Name','LGR del sistema de control 2.1, con controlador P');
rlocus(L)

% Segundo intento
Kp = 3.77;
C = Kp*(1 + 20/49*s);

% FTLA
L = P2*C;

figure('Name','LGR del sistema de control 2.1, con controlador PD');
rlocus(L)

figure('Name','Respuesta del sistema de control 2.1 con controlador PD');
step(feedback(L, 1))


%% 2.2
%Kp = 3.077
%Td = 0.737
%Ti = 1/sqrt(0.9)

% Primer intento
Kp = 1;
C = Kp*(1 + 1/(1/sqrt(0.9)*s));

% FTLA
L = minreal(P2*C);

figure('Name','LGR del sistema de control 2.2, con controlador PI');
rlocus(L)

% Segundo intento
Kp = 3.077;
C = Kp*(1 + sqrt(0.9)/s)*(1 + 0.737*s);

% FTLA
L = minreal(P2*C);

figure('Name','LGR del sistema de control 2.2, con controlador PID serie');
rlocus(L)

figure('Name','Respuesta del sistema de control 2.1 con controlador PD');
step(feedback(L, 1))

%% 3

%Utilizaci�n de sisotool
sisotool(P1)
sisotool(P2)


