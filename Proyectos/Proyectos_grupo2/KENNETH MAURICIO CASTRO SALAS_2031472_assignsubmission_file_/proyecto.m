 %% Identificacion
 s=tf('s');
 P=(1.59*exp(-61*s))/(28*s+1);
 P1=(1.59*exp(-61.0828*s))/(27.846*s+1);
 P2=(1.59*exp(-58.922*s))/((27.584*s+1)*(1.9789*s+1));
 
 figure(1)
 t = (0:0.001:250);
 u=heaviside(t);
lsim(P, u, t, 'b')
  xlabel('Tiempo')
 ylabel('Amplitud')
 
 
 figure(2)
 hold on
 lsim(P, u, t, 'b')
 lsim(P1, u, t, 'g')
 lsim(P2, u, t, 'r')
 title('Gráfica Comparativa de modelos con el Sistema Real')
 xlabel('Tiempo')
 ylabel('Amplitud')
 legend ('Sistema Real', 'Modelo de POMTM', 'Modelo de SOMTM')
 hold off
 
 %% calculo de indices IAE para identificacion
 
 
 
 e1=lsim(P-P1,u,t);
 IAE1=trapz(t,abs(e1))
 
 e2=lsim(P-P2,u,t);
 IAE2=trapz(t,abs(e2))
 
 
%% Sisotool controlador
 Psis=(1.59)/(27.846*s+1);
 sisotool(Psis);

 %% Fragilidad
  figure(3)
 Psist=(1.59)/(27.846*s+1);
 %PI
Kp=0.361081;
Ti=59.9787;
Td=0;
 fragility_rings(Kp,Ti,Td,P1)
 
 figure(4)
 %PID
 Kp2=0.42630;
Ti2=58.8874;
Td2=14.6785;
 fragility_rings(Kp2,Ti2,Td2,P2)
 
 
 %% IAE para controladores
 Preal=(1.59*exp(-61*s))/(28*s+1);
 Paprox=(1.59*exp(-61.0828*s))/(27.846*s+1);
 
 PI=0.361081*(1+1/(58.9787*s));
 PID=0.4263*(1+1/(58.8874*s)+14.6785*s);
 
 MYrealPI=(Preal*PI)/(1+Preal*PI);
 MYaproxPI=(Paprox*PI)/(1+Paprox*PI);
 e11=lsim(MYrealPI-MYaproxPI, u, t);
 IAE_PI=trapz(t,abs(e11))
 
 MYrealPID=(Preal*PID)/(1+Preal*PID);
 MYaproxPID=(Paprox*PID)/(1+Paprox*PID);
 e11=lsim(MYrealPID-MYaproxPID, u, t);
 IAE_PID=trapz(t,abs(e11))
 


 
 
 
 
 
 