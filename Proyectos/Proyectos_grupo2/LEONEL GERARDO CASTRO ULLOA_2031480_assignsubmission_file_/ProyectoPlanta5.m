clear all
close all
clc
%% Indentificaci�n del modelo
s=tf('s');
P=-exp(-30*s)*(1+0.4*exp(-45*s))/(40*s+1);
Ho=-1.4*exp(-37.151*s)/(45.627*s+1);
PLeo=-1.4*(exp(-32.4*s))/(51.6*s+1);
figure
t=0:0.1:400;
u=-1*heaviside(t-1);
lsim(P,u,t)
hold
plot(t,u,'-g')
%% Indeces de error IAE e ISE
figure
grid on
hold
plot(t,u,'-g')

eHO=lsim(Ho,u,t);
eLeo=lsim(PLeo,u,t);
eP=lsim(P,u,t);

IAEHo=trapz(t,abs(eP-eHO));
IAELeo=trapz(t,abs(eP-eLeo));
ISEHo=abs(trapz(t,(eP-eHO).^2));
ISELeo=trapz(t,(eP-eLeo).^2);

%% Anillos de fragilidad
% Para la planta Ho
% anal�tica
figure
fc_fragility_rings(-0.4425,45.6227,0,Ho)
% Klan
figure()
fc_fragility_rings(-0.6208,53.25991,0,Ho)
% trydus
figure()
fc_fragility_rings(-0.5263,52.631,0,Ho)

%% Margenes de ganancia
%C(s) por s�ntesis anal�tica
CA=-0.4425*(45.627*s+1)/(45.627*s);
%C(s) Klan
CK=-0.6208*(53.25991*s+1)/(53.25991*s);
%C(s) trydus
Ct=-0.5263*(52.631*s+1)/(52.631*s);

[GAm,PAm]=margin(CA*Ho);
[GKm,PKm]=margin(CK*Ho);
[Gtm,Ptm]=margin(Ct*Ho);

%% Esfuerzo de control
plot(Klan)
hold
plot(Trydus)
grid on
title('Se�al del controlador')
ylabel('u(t)')
xlabel('t [s]')
tv2=0;
tv3=0;
  
for q=1:1:length(Klan)-1;
    tv2=tv2+abs(Klan(q+1)-Klan(q))
end
for w=1:1:length(Trydus)-1;
    tv3=tv3+abs(Trydus(w+1)-Trydus(w))
end    
