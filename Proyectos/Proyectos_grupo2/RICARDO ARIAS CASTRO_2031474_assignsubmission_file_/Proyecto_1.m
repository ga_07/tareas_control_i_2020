%% Ricardo Arias Castro, B60633
%% María Isabel Blanco, B61064
%% Daniel Corrales Venegas, B62174

%Primer archivo

%código empleado para simular y estimar el sistema, para la implemetación
%de los métodos de sintonización y para la simulacion del sistema de lazo
%cerrado empleando los valores de los métodos

clear; 
clc;

s=tf('s');
%% Curva estática

%sistema real, obtenido del enunciado del proyecto
Pteo=(1.59*exp(-61*s))/(28*s+1);
%vectro de tiempo para la simulación
t=0:0.5:300;
%entrada
u=heaviside(t);
%salida
yt=lsim(Pteo,u,t);

%% Estimación, método de Alfaro

%sistema identificado
Pexp=(1.59*exp(-61.01*s))/(27.97*s+1);
%salida ante la misma entrada del sistema real
ye=lsim(Pexp,u,t);
%gráfico de ambas
plot(t,yt,t,ye);

%% Estimación, método de system identification

%se trapone el vector de entrada para que sea compatible
u=u';
%objeto de datos para la estimación, con los datos de entrada y salida del
%sistema real
est=iddata(yt,u,0.5);
%estimación en base al objeto de datos anterior, para un modelo de primer
%orden con tiempo muerto
sist=procest(est,'P1D');
%se comparan ambos sistemas resultantes
compare(est,sist);
xlabel('Tiempo (s)');
ylabel('Magnitud');

%% Sistema utilizado

%Parámetros de la planta
K=1.59;
L=61.01;
T=27.97;
to=L/T;

%planta identificada, sin el retardo L
P=K/(T*s+1);

%% Método de sintonización analítica 
%utilizando el predictor de Smith para corregir el alto tiempo de retardo

tc=1; %otros posible valores de tc: 1.9; 1.5; 1.7; 0.7

%Parámetros del controlador
Kp=(2-tc)/(K*tc);
Ti=T*tc*(2-tc);
Td=0;

%se utiliza la función fragility rings proporcionada por la cátedra
figure
fc_fragility_rings(Kp,Ti,Td,P);

%Se abre y se simula el sistema real, con su tiempo muerto y el predictor 
%de Smith, empleando los valores obtenidos de la sintonización para el
%controlador PI
open_system('SistControl.slx');
sim('SistControl.slx');

%se grafica la salida, la señal de control y la perturbación, del sistema
%real
figure
plot(Ts,Ys,Ts,Ds,Ts,Us);
xlabel('Tiempo (s)');
ylabel('Salida (%)');

%% Método de Hazebroek p.30 Handbook 
%utilizando el predictor de Smith para corregir el alto tiempo de retardo

%Parámetros del controlador
x1=1.28;
x2=0.95;
Kp=(x1*T)/(K*L);
Ti=x2*T;
Td=0;

%se utiliza la función fragility rings proporcionada por la cátedra
figure
fc_fragility_rings(Kp,Ti,Td,P);

%Se simula el sistema real, con su tiempo muerto y el predictor de Smith, 
%empleando los valores obtenidos de la sintonización para el controlador PI
sim('SistControl.slx');

%se grafica la salida, la señal de control y la perturbación, del sistema
%real
figure
plot(Ts,Ys,Ts,Ds,Ts,Us);
xlabel('Tiempo (s)');
ylabel('Salida (%)');

%% Método de Reswick p.31 Handbook 
%utilizando el predictor de Smith para corregir el alto tiempo de retardo
%este método no fué seleccionado como candidato para la selección

%Parámetros del controlador
Kp=0.2/K;
Ti=0.2*L;
Td=0;

%se utiliza la función fragility rings proporcionada por la cátedra
figure
fc_fragility_rings(Kp,Ti,Td,P);

%Se simula el sistema real, con su tiempo muerto y el predictor de Smith, 
%empleando los valores obtenidos de la sintonización para el controlador PI
sim('SistControl.slx');

%se grafica la salida, la señal de control y la perturbación, del sistema
%real
figure
plot(Ts,Ys,Ts,Ds,Ts,Us);
xlabel('Tiempo (s)');
ylabel('Salida (%)');

%% Método de Marlin p.81 Handbook
%utilizando el predictor de Smith para corregir el alto tiempo de retardo

%Parámetros del controlador
Kp=0.45/K;
Ti=0.55*T;
Td=0.14*T;

%se utiliza la función fragility rings proporcionada por la cátedra
figure
fc_fragility_rings(Kp,Ti,Td,P);

%Se abre y se simula el sistema real, con su tiempo muerto y el predictor 
%de Smith, empleando los valores obtenidos de la sintonización para el
%controlador PID
open_system('SistControlPID.slx');
sim('SistControlPID.slx');

%se grafica la salida, la señal de control y la perturbación, del sistema
%real
figure
plot(Ts,Ys,Ts,Ds,Ts,Us);
xlabel('Tiempo (s)');
ylabel('Salida (%)');