%% Ricardo Arias Castro, B60633
%% María Isabel Blanco, B61064
%% Daniel Corrales Venegas, B62174

%Primer archivo

%código empleado para obtener los resultados de las simulaciones y apartir
%de ellos, obtener los índices de desempeño como índices integrales de
%error, variación total de control y además, los gráficos de la respuesta
%temporal con los valores normalizados entre 0 y 100%

clear; 
clc;

%Parámetros de la planta
K=1.59;
L=61.01;
T=27.97;

%% Método de sintonización analítica 

tc=1;

%Parámetros del controlador
Kp=(2-tc)/(K*tc);
Ti=T*tc*(2-tc);

%Se abre y se simula el sistema real, con su tiempo muerto y el predictor 
%de Smith, empleando los valores obtenidos de la sintonización para el
%controlador PI
open_system('SistControl.slx');
sim('SistControl.slx');

%Variación total de control
TVu=0;
for i = 20000:60000
    TVu= TVu + abs(Us(i+1)-Us(i));
end

%se imprime la variación total de control
TVu

%normalización
Ys=Ys*100;
Us=Us*100;
Ds=Ds*100;

%se grafica la salida con las unidades normalizadas
figure
plot(Ts,Ys,'r-',Ts,Ds,'c',Ts,Us,'r--');
xlabel('Tiempo (s)');
ylabel('Salida (%)');

%se calculan los índices de error, entre el 200 y 600 segundos, por los
%motivos expuestos en el documento escrito
Ys=Ys(20001:60001);
Ds=Ds(20001:60001);
Ts=Ts(20001:60001);
IAE=sum(abs(Ys-Ds))
ITAE=sum(Ts.*abs(Ys-Ds))

%% Método de Hazebroek p.30 Handbook

%Parámetros del controlador
x1=1.28;
x2=0.95;
Kp=(x1*T)/(K*L);
Ti=x2*T;

%Se simula el sistema real, con su tiempo muerto y el predictor de Smith, 
%empleando los valores obtenidos de la sintonización para el controlador PI
sim('SistControl.slx');

%Variación total de control
TVu=0;
for i = 20000:60000
    TVu= TVu + abs(Us(i+1)-Us(i));
end

%se imprime la variación total de control
TVu

%normalización
Ys=Ys*100;
Us=Us*100;
Ds=Ds*100;

%se grafica la salida con las unidades normalizadas
figure
plot(Ts,Ys,'g-',Ts,Ds,'c',Ts,Us,'g--');
xlabel('Tiempo (s)');
ylabel('Salida (%)');

%se calculan los índices de error, entre el 200 y 600 segundos, por los
%motivos expuestos en el documento escrito
Ys=Ys(20001:60001);
Ds=Ds(20001:60001);
Ts=Ts(20001:60001);
IAE=sum(abs(Ys-Ds))
ITAE=sum(Ts.*abs(Ys-Ds))

%% Método de Marlin p.81 Handbook

%Parámetros del controlador
Kp=0.45/K;
Ti=0.55*T;
Td=0.14*T;

%Se abre y se simula el sistema real, con su tiempo muerto y el predictor 
%de Smith, empleando los valores obtenidos de la sintonización para el
%controlador PID
open_system('SistControlPID.slx');
sim('SistControlPID.slx');

%Variación total de control
TVu=0;
for i = 20000:60000
    TVu= TVu + abs(Us(i+1)-Us(i));
end

%se imprime la variación total de control
TVu

%normalización
Ys=Ys*100;
Us=Us*100;
Ds=Ds*100;

%se grafica la salida con las unidades normalizadas
figure
plot(Ts,Ys,'m-',Ts,Ds,'c',Ts,Us,'m--');
xlabel('Tiempo (s)');
ylabel('Salida (%)');

%se calculan los índices de error, entre el 200 y 600 segundos, por los
%motivos expuestos en el documento escrito
Ys=Ys(20001:60001);
Ds=Ds(20001:60001);
Ts=Ts(20001:60001);
IAE=sum(abs(Ys-Ds))
ITAE=sum(Ts.*abs(Ys-Ds))