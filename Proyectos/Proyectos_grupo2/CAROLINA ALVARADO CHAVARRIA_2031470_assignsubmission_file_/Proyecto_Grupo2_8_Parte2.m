%Proyecto de Sistema de Control
%Grupo 2-8
%Carolina Alvarado Chavarría
%Robin González Ricz
%Douglas González Parra
%Es necesario correr el simulink primero y se necesita fc_fragility_rings.p
%provisto por los profesores

%% robustez y fragilidad, esfuerzo.
clc;


%% Toma señales de out de scope 

st = out.chen.Time; % senal tiempo
schen = out.chen.Data; % datos chen
schen = schen/norm(schen);
svelazquez = out.velazquez.Data; % datos velazquez
svelazquez = svelazquez/norm(svelazquez);

%ssimple = out.simple.Data; % PID simple
%ssimple = ssimple/norm(ssimple);

sd = out.d.Data; % perturbacion 
sd = sd/norm(sd);

sr = out.r.Data; % referencia control
sr = sr/norm(sr);

su_d_chen = out.u_d_chen.Data; % señal u perturbacion chen
su_d_chen = su_d_chen/norm(su_d_chen);

su_d_velazquez = out.u_d_velazquez.Data; % señal u perturbacion velazquez
su_d_velazquez = su_d_velazquez/norm(su_d_velazquez);

%su_d_simple = out.u_d_simple.Data; % señal u perturbacion simple
%su_d_simple = su_d_simple/norm(su_d_simple);


figure(4)
plot(st,schen,st,svelazquez,st,sr,st,sd)
%plot(st,schen,st,svelazquez,st,sr,st,sd,st,ssimple)
xlim([0 650])
title('Respuesta ante ambios en la señal de referencia y ante peturbaciones')
xlabel('Tiempo(s)')
ylabel('Medido en  %')
legend('Chen','Velázquez','referencia','perturbación')
grid on

%% toma la parte de servo
srm = sr(2:166);
schen_servo = schen(2:166);
velazquez_servo = svelazquez(2:166);
%simple_servo = ssimple(2:166);

%% toma la parte de la perturbación
sdm=sd(132:186);
sudmc=su_d_chen(132:186);
sudmv=su_d_velazquez(132:186);
%sudms=su_d_simple(132:186);
schen_regulatorio = schen(132:186);
velazquez_regulatorio = svelazquez(132:186);
%simple_regulatorio = ssimple(132:192);

%% errores regulatorio chen
IAE2_d_c=sum(abs(sdm - schen_regulatorio));
ISE2_d_c=sum((sdm - schen_regulatorio).^2);
disp('Indice error regulatorio Chen');
display(IAE2_d_c);
display(ISE2_d_c);

%% errores regulatorio velazquez
IAE2_d_v=sum(abs(sdm - velazquez_regulatorio));
ISE2_d_v=sum((sdm - velazquez_regulatorio).^2);
disp('Indice error regulatorio Velázquez');
display(IAE2_d_v);
display(ISE2_d_v);

%% errores regulatorio simple
%IAE2_d_s=sum(abs(sdm - simple_regulatorio));
%ISE2_d_s=sum((sdm - simple_regulatorio).^2);
%display('Indice error regulatorio PID simple')
%display(IAE2_d_s);
%display(ISE2_d_s);

%% errores servo chen
IAE2_r_c=sum(abs(srm - schen_servo));
ISE2_r_c=sum((srm - schen_servo).^2);
disp('Indice error servo Chen');
display(IAE2_r_c);
display(ISE2_r_c);

%% errores servo velazquez
IAE2_r_v=sum(abs(srm - velazquez_servo));
ISE2_r_v=sum((srm - velazquez_servo).^2);
disp('Indice error servo velazquez')
display(IAE2_r_v);
display(ISE2_r_v);

%% errores servo simple
%IAE2_r_s=sum(abs(srm - simple_servo));
%ISE2_r_s=sum((srm - simple_servo).^2);
%display('Indice error servo simple')
%display(IAE2_r_s);
%display(ISE2_r_s);


%% esfuerzo control regulador chen

for i = 1:length(sudmc)-1
    Esfuerzo_d_chen = sum(abs(sudmc(i+1)-sudmc(i)));
end 

disp('Esfuerzo de control regulatorio')
display(Esfuerzo_d_chen)

%% esfuerzo control regulatorio velezquez

for i = 1:length(sudmv)-1
    Esfuerzo_d_velazquez = sum(abs(sudmv(i+1) - sudmv(i)));
end 

disp('Esfuerzo de control regulatorio velazquez')
display(Esfuerzo_d_velazquez)

%% esfuerzo control regulatorio pid simple

%for i = 1:length(sudms)-1
 %   Esfuerzo_d_simple = sum(abs(sudms(i+1) - sudms(i)));
%end 

%display('Esfuerzo de control regulatorio simple')
%display(Esfuerzo_d_simple)

%% robuztes fragilidad con fc_fragility_rings(kp,Ti,Td,P)
figure(2)
fc_fragility_rings(Kc,Tau_i,Tau_d,Pi2) % robustez fragilidad chen

figure(3)
fc_fragility_rings(Kc_1,Tau_i_1,Tau_d_1,Pi2) %  robustezfrailidad velezquez


%figure(4)
%fc_fragility_rings(Kp,Ti,Td,Pi2) % robustez frailidad simple
