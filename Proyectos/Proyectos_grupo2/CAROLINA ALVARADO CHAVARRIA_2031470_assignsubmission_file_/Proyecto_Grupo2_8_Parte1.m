%Proyecto de Sistema de Control
%Grupo 2-8
%Carolina Alvarado Chavarría
%Robin González Ricz
%Douglas González Parra
%Se corre este código primero para que el archivo de simulink pueda
%reconocer las variables que se le inserto.

clc;


%% Modelo de sistema bola en viga
s=tf('s');
P=0.7/s^2;
Tfinal=50;

%% Respuesta a escalón
[y,t] = step(P,Tfinal);
figure(1);
plot(t,y); hold on;
r=(ones(101,1))';
plot(t,r,'y');
grid on;
title('Curva de reacción de proceso integrante');
xlabel('Tiempo(s)');
ylabel('y, r');
legend('y','r');
 
%% Curva tangente
h = mean(diff(t));
dy = gradient(y, h);                                            % Numerical Derivative
[~,idx] = max(dy);                                              % Index Of Maximum
b = [t([idx-2,idx-1]) ones(2,1)] \ y([idx-2,idx-1]);            % Regression Line Around Maximum Derivative
tv = [-b(2)/b(1); (1-b(2))/b(1)];                               % Independent Variable Range For Tangent Line Plot
f = [t ones(size(t))] * b;                                      % Calculate Tangent Line
figure(2);
plot(t, y);
hold on;
plot(t, f, '-r');                                               % Tangent Line
hold on;
plot(t,r,'y');
hold off;
grid on;
title('Curva de reacción de proceso integrante con recta tangente');
xlabel('Tiempo(s)');
ylabel('y, r');
legend('y','Recta tangente','r');  

%% Analisis de las aproximaciones
Pi1=35*exp(-25*s)/(s);         
Pi2=35*exp(-8*s)/(s*(17*s+1));          
[y1,t1] = step(Pi1, Tfinal);
[y2,t2] = step(Pi2, Tfinal);
figure(3);
plot(t,y,t1,y1,t2,y2);
grid on;
title('Aproximación de respuestas de proceso integrante');
xlabel('Tiempo(s)');
ylabel('Amplitud');
legend('y','Pi1','Pi2');
axis([0 50 0 900]);

%% Indices de error
IAE1=sum(abs(y-y1));
ISE1=sum((y-y1).^2);
display(IAE1);
display(ISE1);
IAE2=sum(abs(y-y2));
ISE2=sum((y-y2).^2);
display(IAE2);
display(ISE2);

%% Modelo aproximado de planta
K=35;
L=8;
Tau=17;
Pi2=K/(s*(Tau*s+1));

%Sintonización Chen y Seborg
Tau_c=1.5;%1
Kc=(1/K)*((3*Tau_c+L)*(Tau+L))/(Tau_c+L)^3;
Tau_i=3*Tau_c+L;
Tau_d=(3*Tau_c^2*Tau+3*Tau_c*Tau*L^2+Tau*L^2-Tau_c^3)/((3*Tau_c+L)*(Tau+L));

%Sintonización Velázquez
Kc_1=(0.062/K)*(Tau/L)^0.717;
Tau_i_1=3.537*Tau*(L/Tau)^0.951;
Tau_d_1=1.458*Tau*(L/Tau)^0.754;

%Chen Como regulador
C_Chen=Kc*(1+1/(Tau_i*s)+Tau_d*s);
Myd_Chen=(Tau_i*s*exp(-L*s))/(Kc*(Tau_c*s+1)^3);
[y3,t3] = step(Myd_Chen);
