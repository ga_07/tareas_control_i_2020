%% Codigo proyecto 
% Grupo 6: B73273, B76222 y B67846. 
close all
clear all
clc
%% Obtención de los datos de salida del sist. real.
t = 0:0.01:15;
H = heaviside(t);
s = tf('s');
P = (-2.55*(s-1.176))/(s+2)^2;
R = feedback(P,1,-1);
[y,x] = lsim(P,H,t);
figure(1);
plot(t,y);
title('Respuesta proceso real')
xlabel('Tiempo (s)');
ylabel('Amplitud');

%% Identificación de modelos 
% Del método de lazo abierto se obtiene lo siguiente:
% Donde: T=0.654; b=1.3146; a=0.5888 y L=0. 
P1 = (0.75*(-0.8597*s+1))/((0.654*s+1)*(0.3851*s+1));
[y1,x1] = lsim(P1,H,t);
% sisotool(P1);

% Del metodo de Ho, dos puntos se obtiene:
% El tiempo muerto se modela utilizando la aproximacion de Pade.
P2 = (0.7523*(-0.4727*s+1))/((0.6734*s+1)*(0.4727*s+1));
[y2,x2] = lsim(P2,H,t);

% Comparacion modelos obtenidos 
figure(2);
plot(t,y(:,1))
title('Respuesta de los diversos modelos ante una entrada escalon')
xlabel('Tiempo (s)');
ylabel('Amplitud');
hold on
plot(t,y1(:,1))
plot(t,y2(:,1))
hold off



%% Indices de desempeno
% Para el primer modelo
Ye1 = y-y1;
Ya1 =abs (Ye1);
Yc1 =Ye1.^2;
IAE1=sum(Ya1)
ISE1=sum(Yc1)

% Para el segundo modelo
Ye2 = y-y2;
Ya2=abs (Ye2);
Yc2=Ye2.^2;
IAE2=sum(Ya2)
ISE2=sum(Yc2)

%% Primer algoritmo de control
% Para el caso de los diagramas empleados, se deberá correr el primer
% diagrama de bloques y correr el codigo en esta seccion ya que los datos
% se reescriben para el segundo diagrama
% Se utiliza un algoritmo de control PID 
% Donde Kp = 0.4823; Ti = 0.85538 y Td = 0.2313
Cy1 = (0.10477*s^2+0.4229*s+0.4823)/(0.019748*s^2+0.8538*s);
Cr1 = (0.4823*s+0.56488)/(s);

% Fragilidad y Robustez
figure(3);
fc_fragility_rings(0.4823,0.8538,0.2313,P1);

% Simulaci\[OAcute]n con datos del Workspace (Simulink) 
figure(4);
plot (out.ys1.Time,out.ys1.Data);
hold on;
xlabel ('Tiempo (s)');
ylabel ('Amplitud (%)');
title ('Sistema con controlador PID (1GdL)');
plot (out.rs1.Time,out.rs1.Data);
plot (out.ds1.Time,out.ds1.Data);
hold off;
figure(5);
plot(out.us1.Time,out.us1.Data);
xlabel ('Tiempo (s)');
ylabel ('Amplitud');
title ('Señal de control u (s)');

% Esfuerzo de control 
Uyr1 = Cr1/(1+(Cy1*P1));
Uyd1 = -(Cy1*P1)/(1+(Cy1*P1));
Vyr1 = step(Uyr1,t);
Vyd1 = step(Uyd1,t);

TVur1 = sum(abs(diff(Vyr1)))
TVud1 = sum(abs(diff(Vyd1)))

% Desempeño

% Calculo del IAE
iae1=sum(abs(out.rs1.Data -  out.ys1.Data)); % Calculo de IAE
disp("El valor del IAE es:")
disp(iae1)

% Calculo del Mp
mp1=(max(out.ys1.Data)-out.ys1.Data(end))*100/(out.ys1.Data(end)-out.ys1.Data(1));
disp("El valor del Mp es:")
disp(mp1)


%% Segundo Algoritmo de control
% Correr el segundo diagrama de bloques y luego correr esta sección
% Se utiliza un PI 
% Donde Kp = ; Ti =  y Td = 
Cy2 = (0.52839*s+0.56784)/(s);
Cr2 = (0.54318*s+0.56784)/(s);
sys2 = 1/(1+Cy2*P1);

% Fragilidad y Robustez
figure(6);
fc_fragility_rings(0.52839,0.93052,0,P1);

% Simulación con datos del Workspace (Simulink)
figure(7);
plot (out.ys2.Time,out.ys2.Data);
hold on;
xlabel ('Tiempo (s)');
ylabel ('Amplitud (%)');
title ('Sistema con controlador PI (2GdL)');
plot (out.rs2.Time,out.rs2.Data);
plot (out.ds2.Time,out.ds2.Data);
hold off;
figure(8);
plot(out.us2.Time,out.us2.Data);
xlabel ('Tiempo (s)');
ylabel ('Amplitud (%)');
title ('Señal de control u (s)');

% Esfuerzo de control 
Uyr2 = Cr2/(1+(Cy2*P1));
Uyd2 = -(Cy2*P1)/(1+(Cy2*P1));
Vyr2 = step(Uyr2,t);
Vyd2 = step(Uyd2,t);

TVur2 = sum(abs(diff(Vyr2)))
TVud2 = sum(abs(diff(Vyd2)))

% Desempeño

% Calculo del IAE
iae2=sum(abs(out.rs2.Data -  out.ys2.Data)); % Calculo de IAE
disp("El valor del IAE es:")
disp(iae2)

% Calculo del Mp
mp2=(max(out.ys2.Data)-out.ys2.Data(end))*100/(out.ys2.Data(end)-out.ys2.Data(1));
disp("El valor del Mp es:")
disp(mp2)

