
%% Controlador Astrom & Hagglund
clc 

close all

s=tf("s");

p=0.7/s^2;

C_aston =(7.53*s^2 + 10.74*s + 12.23)/(0.8785*s)

L1 = C_aston*p;

L11 = 1/(1+L1) 

margin(L1);

bode(L11)

grid on
%entonces note que el margen de ganancia es -14.4dB a los 1.27rad/s
%note que el margen de fase es 75.8° a los 5.9 rad/s
%% Controlador Skogestad
clc 

close all

s=tf("s");

p=0.7/s^2;

C_sko = (5.714*s^2 + 28.57*s + 35.71)/(0.00016*s^2 + 0.4*s)

L2 = C_sko*p;

L22 = 1/(1+L2) 

margin(L2);

bode(L22)

grid on
%entonces note que el margen de ganancia es -18 dB a los 2.5rad/s
%note que el margen de fase es 63.1° a los 10.6 rad/s

