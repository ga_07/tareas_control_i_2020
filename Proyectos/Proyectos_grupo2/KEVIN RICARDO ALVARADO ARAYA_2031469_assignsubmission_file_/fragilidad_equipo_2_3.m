% Planta

%s=tf("s");
%p=0.7/s^2;

% Controlador Astrom & Hagglund
%C_aston=(7.53*s^2 + 10.74*s + 12.23)/(0.8785*s)

% Controlador Skogestad
%C_sko= (5.714*s^2 + 28.57*s + 35.71)/(0.00016*s^2 + 0.4*s)

%% Entonces tenemos para el controlador Skogestad
clc
%como en el handbook se encontro un Ti = 0.4000 y para las simulaciones se
%usa Ti = 1/0.4000 entonces usamos este segundo valor de tiempo integral
s=tf("s");
Ti = 1/(0.4000); 
Td = 0.4000;
Kp = 35.7143;
p=0.7/s^2;
fc_fragility_rings(Kp,Ti,Td,p)
%respecto a las respuestas
%con Ti = 0.4000
%Mso = 1.33
%RFI = 0.116
%con Ti = 1/0.4000
%Mso = 1.32
%RFI = 0.113
%ahora con estos últimos resultados que podemos decir?
%entonces observemos que Ms = 2
%y esta centrado en -1
%ahora observando la gráfica polar podemos ver que 
%primero los controladores se clasifican en elasticos, no fragiles y
%fragiles
%entonces si observamos la gráfica polar el MSnominal es el circulo azul
%punteado y el caso donde se pierde el 10% de robustez que equvale a 1.1Msnomial
%ese circulo es el verde/negro entonces entre la azul punteada y la verde/negra hay un anillo
%entonces obsevamos la gráfica polar roja si estamos midiendo el inidice de
%sensibilidad max 
%entonces vemos el punto de tangencia y como está fuera de ese anillo
%podemos determinar que el controlador no es elastico. Es un controlador no
%fragil en su robustez ya que su punto de tangencia se encuentra dentro del
%anillo 1.5Msnominal.
%ademas podemos observar lo siguiente:1
%Tenemos un RFI = 0.113
%entonces tenemos FI20 ≤ 0.50 entonces al modificar los parametros del
%controlador si se origina una perdida de robustez pero el valor todavia
%esta en un rango aceptable. Por lo tanto no es frágil en la robustez.


%% %% Entonces tenemos para el controlador Astrom & Hagglund
clc
s=tf("s");


Ti = 1/(0.8785);
Td = 0.7010;
Kp = 12.2286;
p=0.7/s^2;
fc_fragility_rings(Kp,Ti,Td,p)
%respecto a las respuestas
%con Ti = 0.8785
%Mso = 1.34
%RFI = 0.115
%con Ti = 1/0.8785
%Mso = 1.34
%RFI = 0.115
%ahora con estos últimos resultados que podemos decir?
%entonces observemos que Ms = 2
%y esta centrado en -1
%ahora observando la gráfica polar podemos ver que 
%primero los controladores se clasifican en elasticos, no fragiles y
%fragiles
%entonces si observamos la gráfica polar el MSnominal es el circulo azul
%punteado y el caso donde se pierde el 10% de robustez que equvale a 1.1Msnomial
%ese circulo es el verde/negro entonces entre la azul punteada y la verde/negra hay un anillo
%entonces obsevamos la gráfica polar roja si estamos midiendo el inidice de
%sensibilidad max 
%entonces vemos el punto de tangencia y como está fuera de ese anillo
%podemos determinar que el controlador no es elastico. Es un controlador no
%fragil en su robustez ya que su punto de tangencia se encuentra dentro del
%anillo 1.5Msnominal.
%ademas podemos observar lo siguiente:
%Tenemos un RFI = 0.115
%entonces tenemos FI20 ≤ 0.50 entonces al modificar los parametros del
%controlador si se origina una perdida de robustez pero el valor todavia
%esta en un rango aceptable. Por lo tanto no es frágil en la robustez.


