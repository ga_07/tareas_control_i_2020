%% Identificaciones Equipo 2-3

%% Identificación para procesos integrantes
clc
clear all

s = tf('s');

y2 = 3.159e+05;
x2 = 950;
y1 = 8.750e+04;
x1 = 500;

m = (y2-y1)/(x2-x1);

b = y1 - m*x1;

Lprima = -b/m;

y_LT = 3.756e+04;

T = (2.718 * y_LT)/(m);

L = Lprima - T;

%L = 2.82;

K = m;

Pest1er = (K*exp(-Lprima*s))/s

Tiempo = 0:0.25:1000;

escalon = heaviside(Tiempo-1);

Poriginal = 0.7/(s^2);

Pest = (K*exp(-L*s))/(s*(T*s+1));

Yorig = lsim(Poriginal, escalon, Tiempo);
Yest = lsim(Pest, escalon, Tiempo);
Yest1er = lsim(Pest, escalon, Tiempo);

figure(1)
plot(Tiempo, Yorig, Tiempo, Yest)

%% Identificación curve fitting

clc
clear all

s = tf('s');

Pr=0.7/s^2;
[y,t]=step(Pr);
figure(2)
plot(t,y)

