% Obtenci�n de gr�ficas y Indices de error 
% Equipo 2-3
sim("modelos_sintonizaciones_equipo_2_3")

t = ans.planta.time;
planta = ans.planta.signals.values;

%% Modelo para plantas integrantes

integrante= ans.integrante.signals.values;

figure(1)
plot(t, planta, "m-", t, integrante, "b-", "LineWidth", 2);
legend("Planta", "Modelo 2", "FontSize", 25);
xlabel("Tiempo (s)", "FontSize", 30);
ylabel("Magnitud (%)", "FontSize", 30);
title('Comparaci�n entre Planta y Modelo 2', "FontSize", 40);
set(gca,'FontSize',20);

%% Modelo para plantas integrantes

curvefit= ans.curvefit.signals.values;

figure(2)
plot(t, planta, "m-", t, curvefit, "b-", "LineWidth", 2);
legend("Planta", "Modelo 1", "FontSize", 25);
xlabel("Tiempo (s)", "FontSize", 30);
ylabel("Magnitud (%)", "FontSize", 30);
title('Comparaci�n entre Planta y Modelo 1', "FontSize", 40);
set(gca,'FontSize',20);
%% Tiempo de simulaci�n en Simulink para los controladores

t = ans.controladores.time;

%% Entradas r(s) y d(s)
r=ans.r.signals.values;
d=ans.d.signals.values;

%% Gr�fica de la planta

figure(3)
plot(t, planta, "m-", "LineWidth", 2)
legend("Planta", "FontSize", 25);
xlabel("Tiempo (s)", "FontSize", 30);
ylabel("Magnitud (%)", "FontSize", 30);
title('Respuesta al escal�n del sistema real a lazo abierto', "FontSize", 40);
set(gca,'FontSize',20)

%% Respuesta del sistema

c_astrom = ans.controladores.signals.values(:,1);
c_skogestad = ans.controladores.signals.values(:,2);

figure(4)
plot(t, r, "g-.", t, d, "g--", t, c_astrom, "k-", t, c_skogestad, "r-", "LineWidth", 2)
legend("r(s)", "d(s)", "Astrom & Hagglund", "Skogestad", "FontSize", 25);
xlabel("Tiempo (s)", "FontSize", 30);
ylabel("Magnitud (%)", "FontSize", 30);
title('Din�mica del sistema', "FontSize", 40);
set(gca,'FontSize',20)

%% Esfuerzos de control
e_astrom = ans.esfuerzo.signals.values(:,1);
e_skogestad = ans.esfuerzo.signals.values(:,2);

figure(5)
plot(t, d, "g--", t, e_astrom, "k-", t, e_skogestad, "r-", "LineWidth", 2)
legend("d(s)", "Astrom & Hagglund", "Skogestad", "FontSize", 25);
xlabel("Tiempo (s)", "FontSize", 30);
ylabel("Magnitud (%)", "FontSize", 30);
title('Esfuerzo de control', "FontSize", 40);
set(gca,'FontSize',20);
axis([9 25 -40 25]);
%% Errores medidos desde la simulaci�n

e1=ans.error1.signals.values;
e2=ans.error2.signals.values;

%% �ndices de errores integrales para el Astrom
iae1 = trapz(t,abs(e1));             % IAE 
ise1 = trapz(t,e1.^2);               % ISE 
itae1 = trapz(t, t.*abs(e1));       % ITAE
itse1 =trapz(t,t.*(e1.^2));         % ITSE

%% �ndices de errores integrales para el Skogestad
iae2 = trapz(t,abs(e2));             % IAE 
ise2 = trapz(t,e2.^2);               % ISE 
itae2 = trapz(t, t.*abs(e2));       % ITAE
itse2 =trapz(t,t.*(e2.^2));         % ITSE