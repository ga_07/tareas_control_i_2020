%% Proyecto de Control

s = tf('s');

%Planta real
P=(3.79*exp(-2*s))/(s*(40*s+1));

t=[0:0.01:1500]; %tiempo
u=heaviside(t-1); %entrada
y=lsim(P,u,t);%salida

%Curva de reacci�n
plot(t,y,'-',t,u,'-')
title('')
xlabel('Tiempo (minutos)');
ylabel('Amplitud (%)')
xlim([0,20])
legend('Salida','Entrada','location','northwest')
grid on;

%Metodo de identificaci�n g�fico
y_tan= 3.79*t-164.88; % tangente

figure;
plot(t,y,'-',t,y_tan,'--')
title('')
xlabel('Tiempo (minutos)');
ylabel('Amplitud (%)')
xlim([0,200])
legend('Salida','Recta tangente','location','northwest')
grid on;

%Primer orden
Pi1= (3.7919*exp(-42.53*s))/(s);
y1=lsim(Pi1,u,t);

%Segundo orden
 Pi2= (3.7919*exp(-1.64*s))/(s*(40.89*s+1));
 y2=lsim(Pi2,u,t);
 
% Identificacion con procest
 Psom = (3.773*exp(-0.014*s))/(s*(40.03*s+1));
 y3 = lsim(Psom,u,t);
 
 Pomm = (3.79)/(s*(39.96*s+1)*(2.019*s+1));
 y4 = lsim(Pomm,u,t);

 figure;
 plot(t,y,'-',t,y1,'-.',t,y2,'k--')
 title('')
 xlabel('Tiempo (minutos)');
 ylabel('Amplitud (%)')
 xlim([0,200])
 legend('Real','IPOMTM','ISOMTM','location','northwest')
 grid on;
 
  figure;
 plot(t,y,'-',t,y3,'-.',t,y4,'k--')
 title('')
 xlabel('Tiempo (minutos)');
 ylabel('Amplitud (%)')
 xlim([0,200])
 legend('Real','SOM','OMM','location','northwest')
 grid on;
  
 % Indices de error
 IAE1=sum(abs(y(1:2001,1)-y1(1:2001,1)))
 IAE2=sum(abs(y(1:2001,1)-y2(1:2001,1)))
 IAE3=sum(abs(y(1:2001,1)-y3(1:2001,1)))
 IAE4=sum(abs(y(1:2001,1)-y4(1:2001,1)))



