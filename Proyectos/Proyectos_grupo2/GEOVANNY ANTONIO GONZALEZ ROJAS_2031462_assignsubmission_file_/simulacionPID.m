clear;
s = tf('s');
%------------------------------------------------------------------------%
%                            Simulacion PID                          %
%------------------------------------------------------------------------%

Kc=0.018;
Ti=124.31;
Td=27.44;

Cy = Kc*(1 + 1/(Ti*s) + (Td*s)/(1 + s*(0.1*Td))); %controlador realimentado
Cr = Kc*(1 + 1/(Ti*s));% + (Td*s)/(1 + s*(Td/10))); %Controlador referencia

P2 = (3.7919*exp(-1.64*s))/(s*(40.89*s+1)); %proceso "real"


%% Tiempos de simulacion
t_span = 0.1;
t_inicio = 0;
t_final = 1600;
t_sim = t_inicio:t_span:t_final;

%Tiempos de activacion entradas
t_r = t_final * 0.3; %activacion del escalon para valor deseado
r0 = 20; %valor inicial de r(t) 50%
dr = 5; %cambio del 15% en r(t)

t_d = t_final * 0.7; %activacion del escalon para perturbacion
d0 = 0; %valor inicial de d(t)
dd = 0.2; %cambio del 5% en d(t)


%% Simulacion y generacion de grafica
sim("bloquesPID", t_sim);

figure;
plot(t_sim, ans.y, '-k',t_sim, ans.r, '-.k',t_sim, ans.d, '--k');
lgd = legend('y(t)', 'r(t)', 'd(t)');
lgd.Location = 'northeast';
xlabel('Tiempo (min)');
ylabel('Amplitud (%)');
xlim([400,1600])
ylim([-1,40])
grid on;

figure;
plot(t_sim, ans.u*100, '-k',t_sim, ans.r, '-.k',t_sim, ans.d, '--k');
legend('u(t)', 'r(t)', 'd(t)');
xlabel('Tiempo (min)');
ylabel('Amplitud (%)');
xlim([400,1600])
ylim([-25,30])
grid on;

%% Desempe�o 
index_r = find(t_sim-t_r == 0);
index_d = find(t_sim-t_d == 0);
index_fin = find(t_sim-t_final == 0);

e_servo = [];
TV_servo = 0;
for i = index_r:index_d  %servo
    e_servo(i-index_r+1) = ans.e(i);
    
    if i ~= index_d %esfuerzo de control
        TV_servo = TV_servo + abs(ans.u(i+1)-ans.u(i));
    end
    
end

e_reg = [];
TV_reg = 0;
for j = index_d:index_fin  %reg
    e_reg(j-index_d+1) = ans.e(j);
    
    if j ~= index_fin %esfuerzo de control
        TV_reg = TV_reg + abs(ans.u(j+1)-ans.u(j));
    end
end

JIAE_servo = trapz(t_r:t_span:t_d, abs(e_servo));
JIAE_reg = trapz(t_d:t_span:t_final, abs(e_reg));

%% Robustez: MS, Am, Phim
L = Cy*P2; %lazo abierto --> (1 - polinomio caracteristico)

[Gm, Phm, Wcg, Wxp] = margin(L);

S = 1/(1+L); %funcion de sensibilidad
[S_Mag, S_Phase] = bode (S); %Obtencion de la magnitud
Ms = max(S_Mag); %Sensibilidad maxima.

%% Fragilidad: RFId20
 figure;
 fc_fragility_rings(Kc,Ti,Td,P2)