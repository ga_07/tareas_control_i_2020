%% IE-0431 Sistemas de Control
% Proyecto Final: modelo de un reactor qu�mico continuamente agitado
% Integrantes: Astryd Gonz�lez-Jos� Pablo Laurent- Joseline S�ncehz
% Grupo 02
% Sub-grupo 1

% A continuaci�n se presenta por secciones el c�digo implementado para el
% an�lisis e identificaci�n de la planta y el controlador dise�ado

% Para cargar los datos de Simulink necesarios para algunos calculos en el
% c�digo, es necesario tener ambos archivos en la misma carpeta
%INSTRUCCIONES:
%Primero abrir el archivo proyectoG02_1.slx
%En el command window de MATLAB ingrese: clear;clc;
%Corra el archivo en simulink proyectoG02_1.slx
%En el command window de MATLAB ingrese: save('datos0')
%Luego corrar el c�digo de MATLAB con normalidad
%% Identificaci�n del sistema real
clear;clc;
load('datos0.mat')
syms kp
t = 0:0.01:10;% vector de tiempo
s = tf('s');%operador s
ui = 1*heaviside(t-0);%se crea un escalon unitario

disp('Parte 1: Identificaci�n')
%PLANTA REAL
sys1 = (0.75*(1-0.85*s))/(0.5*s + 1)^2;
figure(1)
u=100*ui; % para ver las se�ales en porcentaje 
y1=lsim(sys1, u, t);
plot(t, y1, t, u); % para graficar la salida junto con el escal�n
title('Respuesta temporal de la planta real');
ylabel('Y (%)');
xlabel('Tiempo (s)');

% MODELO 1: SEGUNDO ORDEN CON RESPUESTA INVERSA
sys2 = (0.748*(1-0.857*s))/((0.39*s + 1)*(0.648*s + 1));

% MODELO 2: SEGUNDO ORDEN CRITICAMENTE AMORTIGUADO
k = 0.747;
den = [0.2029754788 0.901056 1 ]; %(0.450528*s + 1)^2
sys3 = tf(k, den,'InputDelay',0.871);

%Respuesta escal�n de la planta y los dos modelos
figure(2)
y2=lsim(sys2, u, t);
y3=lsim(sys3, u, t);
plot(t, y1, t, y2, t, y3, t, u);
title('Respuesta temporal de la planta real junto con los modelos');
ylabel('Y (%)');
xlabel('Tiempo (s)');

%Determinaci�n de los errores de los modelos:
ysys1 = lsim(sys1, ui, t) ;
ysys2 = lsim(sys2, ui, t) ;
ysys3 = lsim(sys3, ui, t) ;

% �ndices de error para MODELO DE SEGUNDO ORDEN CON RESPUESTA INVERSA
ISE1 = sum((ysys1 - ysys2).^2)
IAE1 = sum(abs(ysys1 - ysys2))

% �ndices de error para modelo de segundo orden criticamente amortiguado
ISE2 = sum((ysys1 - ysys3).^2)
IAE2 = sum(abs(ysys1 - ysys3))

%% Dise�o de los controladores
disp('Parte 2')
disp('Dise�o del controlador de 2 GdL')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Desempe�o de control para IAE en servomecanismo
%En la variable y12gdl, se encuentra la se�al realimentada del controlador PI de 2Gdl obtenida del bloque workspace.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

y_servo = y12gdl(258:509,1); %en el dato 258 se aplica un escal�n de 10% en el valor deseado, en dato 509 se aplica a la perturbaci�n
ref = 30; %Valor de referencia 30 % cuando servomecanismo tiende a 30% al incrementar el tiempo
disp('IAE/servocontrol')
IAE_servo_2gdl = sum(abs(30 - y_servo))/100 %Error IAE en servomecanismo, al seguir una referencia de 30%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%�ndice de error ISE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('ISE/servocontrol')
ISE_servo_2gdl = sum(abs(30 - y_servo).^2)/100

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Sobrepaso m�ximo PI de 2Gdl servo control
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

M_servo2gdl = max(y_servo);
Mfin = y_servo(end);
disp('Sobrepaso m�ximo servocontrol')
mp_servo = ((M_servo2gdl - Mfin) / Mfin)*100 %(ymax - yf)/yf

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Desempe�o de control para IAE en control regulatorio
% En la variable y12gdl, se encuentra la se�al realimentada del controlador
% PI de 2Gdl obtenida del bloque workspace.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

y_reg = y12gdl(509:810,1);%datos en control regulatorio, se usa referencia de 30%
disp('IAE/control regulatorio')
IAE_reg_2gdl = sum(abs(30 - y_reg))/100 %Error IAE en servomecanismo, al seguir una referencia de 30%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Desempe�o de control para ISE en control regulatorio
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('ISE/control regulatorio')
ISE_reg_2gdl = sum(abs(30 - y_reg).^2)/100

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Evaluaci�n del esfuerzo de control para servomecanismo para el controlador PI 2Gdl.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

u_servo = u12gdl(258:509,1); %datos en servocontrol
u_servo1 = u12gdl(259:510,1); %u+1
disp('Esfuerzo de control servocontrol para controlador PI 2Gdl')
Esf_control_servo_2gdl = sum(abs(u_servo1 - u_servo)) %Esfuerzo de control servomecanismo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Evaluaci�n del esfuerzo de control para Control regulatorio para el controlador PI 2Gdl.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

u_reg = u12gdl(509:809,1);%datos en control regulatorio
u_reg1 = u12gdl(510:810,1);%uk+1
disp('Esfuerzo de control control regulatorio para controlador PI 2Gdl')
Esf_control_regul_2gdl = sum(abs(u_reg1 - u_reg)) %Esfuerzo de control en regulatorio

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Anillo de fragilidad de controlador PI de 2GDL.
% Se mide robustez y estabilidad
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Kp = 0.396608;   %Se puede ser m�s preciso
Ti = 0.83623752; %Se puede ser m�s preciso
Td = 0;
figure(3)
fc_fragility_rings(Kp, Ti, Td, sys2)%Para: sys2 = (0.748*(1-0.857*s))/((0.39*s + 1)*(0.648*s + 1));

% Para Controlador PID 1 Gdl:
disp('Dise�o del controlador de 1 GdL')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Desempe�o de control para IAE en servomecanismo
%En la variable y21gdl, se encuentra la se�al realimentada del controlador
%PID de 1Gdl obtenida del bloque workspace.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

y_servo1G = y21gdl(258:509,1);%datos en servocontrol SE APLICA REFERENCIA 30%
disp('IAE/servocontrol')
IAE_servo_1gdl = sum(abs(30 - y_servo1G))/100%Error IAE en servomecanismo, al seguir una referencia de 30%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Desempe�o de control para ITAE en servomecanismo
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('ISE/servocontrol')
ISE_servo_1gdl = sum(abs(30 - y_servo1G).^2)/100

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Sobre paso PID de 1Gdl control servo control
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

M_sergdl = max(y_servo1G);
Msereg = y_servo1G(end);
disp('Sobrepaso servocontrol')
mp_servo1gdl = ((M_sergdl - Msereg) / Msereg)*100  %(ymax - yf)/yf

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Desempe�o de control para IAE en control regulatorio
%En la variable y21gdl, se encuentra la se�al realimentada del controlador
%PID de 1Gdl obtenida del bloque workspace.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

y_reg1G = y21gdl(509:810,1);%datos en control regulatorio
disp('IAE/control regulatorio')
IAE_reg_1gdl = sum(abs(30 - y_reg1G))/100%Error IAE en servomecanismo, al seguir una referencia de 30%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Desempe�o de control para ISE en control regulatorio
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('ISE/control regulatorio')
ISE_reg_1gdl = sum(abs(30 - y_reg1G).^2)/100

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Evaluaci�n del esfuerzo de control para servomecanismo para el controlador PID 1Gdl.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

u_servo1G = u21gdl(258:509,1);%datos en servocontrol
u_servo1G1 = u21gdl(259:510,1);%uK+1
disp('Esfuerzo de control servocontrol para controlador PID 1Gdl')
Esf_control_servo_1gdl = sum(abs(u_servo1G1 - u_servo1G))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Evaluaci�n del esfuerzo de control para Control regulatorio para el controlador PID 1Gdl.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

u_reg1G = u21gdl(509:809,1);%datos en control regulatorio
u_reg1G1 = u21gdl(510:810,1);%Uk+1
disp('Esfuerzo de control para control regulatorio para controlador PID 1Gdl')
Esf_control_regul_1gdl = sum(abs(u_reg1G1 - u_reg1G)) 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FRAGILITY RINGS PID 1gdl
s = tf('s');%operador s
sys2 = (0.748*(1-0.857*s))/((0.39*s + 1)*(0.648*s + 1));
figure(4)
fc_fragility_rings(0.49382, 0.8604, 0.1582934, sys2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%