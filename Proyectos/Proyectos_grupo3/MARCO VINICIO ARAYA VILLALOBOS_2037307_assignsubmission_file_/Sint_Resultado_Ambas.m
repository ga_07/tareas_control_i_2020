clear all;
close all;
clc

%M�todo Skogestad 2003

%Datos de modelo de la planta
KPR = 3.217;
Tpro=2.461;
Lpro = 0.439;

%Valores del Controlador Lee et al
k1 = 0.2831;
Ti1 = 10000000000; %ES INFINITO
Td1 = 2.54876;



%Par�metros para el calculo del controlador
taoL = Lpro/Tpro;

taoC = taoL; % Velocidad del lazo este es el par�metro de dise�o se recomienda igual a taoL pero puede ser menor

Kcontro = 1 / ((taoC + taoL)*KPR*Tpro);
TIcontrol = 4*(taoC + taoL)*Tpro;
TDcontrol = Tpro;

t = 0:0.1:100;
H=0.2*heaviside(t-2);
H2=0.06*heaviside(t-40);


s=tf('s');

P = (0.7) / (s^2);%Proceso Real
C = Kcontro*(1+1/(TIcontrol*s))*(1+TDcontrol*s);%Controlador
L = P*C; %Lazo Abierto
M = (P*C)/(1+C*P);
M2= (P)/(1+C*P);
y = lsim(M,H,t);
y2= lsim(M2,H2,t);
Y= y;

%Para m�todo de Lee
C1 = k1*(1+(1/(Ti1*s))+(Td1*s)); %Controlador para Lee
L1 = P*C1;
M1 = (P*C1)/(1+C1*P);
M11 =(P)/(1+C1*P);
y1 = lsim(M1,H,t);
y11 = lsim(M11,H2,t);
Y1= y1;
S1 = 1/(1+L1);

figure,
hold on
plot(t,100*Y)
plot(t,100*Y1)
plot(t,100*H,'--')
plot(t,100*H2,':')
xlabel('tiempo [s]');
ylabel('Distancia [m] %');
legend('Skogestad','Lee et all','r(t)','d(t)');



