%Limpiando variables
clear all
clc

%Parámetros
T = 2.461;
L = 0.439;
K = 3.217;
L1 = 2.9 ;

s=tf('s');
P1 = 0.7/s^2
t = 0:0.0001:4; %Tiempo de muestreo 
hl = heaviside(t) ; %Escalón
figure
P1 = lsim(P1,hl,t); 
hold on
plot(t,P1,t,hl);

%Segundo modelo
P2= ( ( K*(exp(-s*L1)) )/(s) )  %IMTM
P2 = lsim(P2,hl,t);
plot(t,P2);

%tercer modelo
P3= ( ( K*(exp(-s*L)) )/ ((s)*( T*s +1  ) ) ) %
P3 = lsim(P3,hl,t);
plot(t,P3);

%Método LIU-GAO
P4 = (3*exp(-2.536*s))/(s)
P4 = lsim(P4,hl,t);
plot(t,P4);


%Método #1 





%Titulación de las gráficas
xlabel('Tiempos [s]')
ylabel('Amplitud [m]')
legend({'Planta','Escalón' ,'IMTM' , 'ISOMTM' , 'LIU-GAO' ,}, 'Location','northwest')
hold off