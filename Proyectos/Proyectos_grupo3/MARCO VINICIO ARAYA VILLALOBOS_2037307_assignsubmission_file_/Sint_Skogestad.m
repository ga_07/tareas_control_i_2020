clear all;
close all;
clc

%M�todo Skogestad 2003

%Datos de modelo de la planta
KPR = 3.217;
Tpro=2.461;
Lpro = 0.439;

%Par�metros para el calculo del controlador
taoL = Lpro/Tpro;

taoC = taoL; % Velocidad del lazo este es el par�metro de dise�o se recomienda igual a taoL pero puede ser menor

Kcontro = 1 / ((taoC + taoL)*KPR*Tpro);
TIcontrol = 4*(taoC + taoL)*Tpro;
TDcontrol = Tpro;

t = 0:0.1:120;
H=heaviside(t-2); %Escal�n en t=2s

s=tf('s');

P = (0.7)/(s^2);%Proceso Real
Pm = 3.217*exp(-0.439*s)/(s*(2.461*s+1)); %Proceso modelado
C = Kcontro*(1+1/(TIcontrol*s))*(1+TDcontrol*s);%Controlador
L = Pm*C; %Lazo Abierto
M = (P*C)/(1+C*P);

y = lsim(M,H,t);
figure,
hold on
plot(t,100*y)
xlabel('tiempo');
ylabel('Distancia %');
legend('y1(t) Skogestad');

S = 1 / (1+L);

%Margen de Fase
figure,
margin(L);

figure,
bodemag(S);

figure,
fc_fragility_rings(Kcontro,TIcontrol,TDcontrol,P)

%C�lculo de IAE e ISE
y1=transpose(y);
e=y1-H;
IAE = trapz(t,abs(e))          %IAE  %3.1677
ISE = trapz(t,e.^2)            %ISE  %1.1039



