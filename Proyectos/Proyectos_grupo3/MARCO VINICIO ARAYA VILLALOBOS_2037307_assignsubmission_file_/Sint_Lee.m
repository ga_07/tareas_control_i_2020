clear all;
close all;
clc

t = 0:0.01:120;
H=heaviside(t-2);

%Valores del Controlador Lee et al
k1 = 0.2831;
Ti1 = 10000000; %ES INFINITO
Td1 = 2.54876;

%Ecuaciones de planta/Controlador
s=tf('s');
P = (0.7)/(s^2); %Proceso Real
Pm = 3.217*exp(-0.439*s)/(s*(2.461*s+1)); %Proceso modelado

%Para m�todo de Lee
C1 = k1*(1+(1/(Ti1*s))+(Td1*s)); %Controlador para Lee
L1 = Pm*C1;
M1 = (P*C1)/(1+C1*P);
y1 = lsim(M1,H,t);
S1 = 1/(1+L1);
%Margen de Fase
figure,
margin(L1);
%Bode
figure,
bodemag(S1);

figure,
fc_fragility_rings(k1,Ti1,Td1,P)


%Respuesta del controlador hasta 120seg
figure,
hold on
plot(t,100*y1);
xlabel('Tiempo (s)');
ylabel('Distancia (%)');
legend(' Lee et al');

%C�lculo de IAE e ISE para Lee
y2 = transpose(y1);
e=y2-H;
IAE = trapz(t,abs(e))          %IAE  %2.6062
ISE = trapz(t,e.^2)            %ISE  %0.9874
