clc 
clear
s=tf('s');
 
%% Para identificaci�n de modelo de orden reducido de la planta P5:
clc
% funcion de transferencia de lazo abierto:
P5 = -(exp(-30*s)*(1+0.4*exp(-45*s)))/(40*s+1);  % planta asignada
t = 0:0.0001:500;
u = 1*heaviside(t-0.000001)*50;
u = transpose(u);
y = (lsim(P5,u,t));      % respuesta temporal del sistma en lazo abierto ante escal�n


figure(1)
plot(t ,y ,t ,u ,'linewidth',2);            % plot entrada - salida
title 'Respuesta del sistema ante escal�n. Pruebas experimentales en el sistema real.';
legend('$y_{P_5}(t)$','$u(t)$','Interpreter','latex','location','northeast')
xlabel ' Tiempo (s) ';
ylabel ' amplitud (%)';
% y_lim_sup = 100;
% y_lim_inf = -100;
% ylim([y_lim_inf, y_lim_sup])
% x_lim_min = -10;
% x_lim_max = 500;
% xlim([x_lim_min, x_lim_max])
grid on;
saveas(gcf,'imagenes/respestaRealConModelo','png')
saveas(gcf,'imagenes/respestaRealConModelo','epsc')

%% Estudio del sistema para obtenci�n de modelo aproximado a la planta:
clc
disp(" ****** MODELADO DEL SISTEMA REAL P5 ****** ")
% Cambios de entrada y salida:
delta_y = y(1) - y(length(y));      % valor de la salida en t -> inf
delta_u = u(1) - u(length(u));      % valor de la entrada en t -> inf
%--------------------------------------------------------------------
%--------------------------------------------------------------------
% Usando el m�todo de 2 puntos para POMTM - M�todo de Viteckov� et al
a = 1.245;
b = 1.498;

% para tiempo en valor de la salida al 33% de su valor final:
y_p1 = 0.33*(y(length(y)));         
indx_p1 = find(y < y_p1 & y > (y_p1-0.001));
t_p1 = t(indx_p1(1));

% para tiempo en valor de la salida al 70% de su valor final:
y_p2 = 0.70*(y(length(y)));         
indx_p2 = find(y < y_p2 & y > (y_p2-0.001));
t_p2 = t(indx_p2(1));

% C�lculos para constantes del modelo:
k_modelo = delta_y/delta_u;
T = a*(t_p2-t_p1);
L = b*t_p1 + (1-b)*t_p2;

% Modelo obtenido 123C:
disp("....................................................")
disp(" -> Modelo POMTM Viteckv�:")
POMTM_vitec = (k_modelo*exp(-L*s))/(T*s+1)
disp("....................................................")

y_POMTM_vitec = lsim(POMTM_vitec,u,t);      % respuesta temporal del POMTM_alfaro123C en lazo abierto ante escal�n
%%
%--------------------------------------------------------------------
%--------------------------------------------------------------------
% Usando el m�todo de 2 puntos para PDMTM - M�todo de Alfaro 123C:
a_ = 0.578;
b_ = 1.555;

% para tiempo en valor de la salida al 25% de su valor final:
y_p1 = 0.25*(y(length(y)));                     % guarda el valor 1 para y
indx_p1 = find(y < y_p1 & y > (y_p1-0.001));    % retorna matr�z con indice donde se encuentra y buscado
t_p1 = t(indx_p1(1));

% para tiempo en valor de la salida al 75% de su valor final:
y_p2 = 0.75*(y(length(y)));                     % guarda el valor 2 para y
indx_p2 = find(y < y_p2 & y > (y_p2-0.001));    % retorna matr�z con indices donde se encuentra valor buscado
t_p2 = t(indx_p2(1));

% C�lculos para constantes del modelo:
k_modelo_ = delta_y/delta_u;
T_ = a_*(t_p2-t_p1);
L_ = b_*t_p1 + (1-b_)*t_p2;

% Modelo obtenido PDMTM 123C:
disp("....................................................")
disp(" -> Modelo PDMTM Alfaro 123C:")
PDMTM_alfaro123C = (k_modelo_*exp(-L_*s))/(T_*s+1)^2
disp("....................................................")


y_PDMTM_alfaro123C = lsim(PDMTM_alfaro123C,u,t);      % respuesta temporal del PDMTM_alfaro123C en lazo abierto ante escal�n
%%
%--------------------------------------------------------------------
%--------------------------------------------------------------------
% Usando el m�todo de 2 puntos para PDMTM - M�todo de Viteckov�:
a_ = 0.794;
b_ = 1.937;

% para tiempo en valor de la salida al 33% de su valor final:
y_p1 = 0.33*(y(length(y)));                     % guarda el valor 1 para y
indx_p1 = find(y < y_p1 & y > (y_p1-0.001));    % retorna matr�z con indice donde se encuentra y buscado
t_p1 = t(indx_p1(1));

% para tiempo en valor de la salida al 70% de su valor final:
y_p2 = 0.70*(y(length(y)));                     % guarda el valor 2 para y
indx_p2 = find(y < y_p2 & y > (y_p2-0.001));    % retorna matr�z con indices donde se encuentra valor buscado
t_p2 = t(indx_p2(1));

% C�lculos para constantes del modelo:
k_modelo_ = delta_y/delta_u;
T_ = a_*(t_p2-t_p1);
L_ = b_*t_p1 + (1-b_)*t_p2;

% Modelo obtenido PDMTM 123C:
disp("....................................................")
disp(" -> Modelo PDMTM Viteckov�")
PDMTM_Vitec = (k_modelo_*exp(-L_*s))/(T_*s+1)^2
disp("....................................................")


y_PDMTM_Vitec = lsim(PDMTM_Vitec,u,t);      % respuesta temporal del PDMTM_alfaro123C en lazo abierto ante escal�n
%%
%--------------------------------------------------------------------
%--------------------------------------------------------------------

% Gr�fica con modelos de prueba:
figure(2)
plot(t ,y, t ,u,t,y_PDMTM_alfaro123C,t,y_POMTM_vitec,t,y_PDMTM_Vitec);
title 'Respuesta del sistema ante escal�n. Pruebas identificaci�n.';
legend('$y_{P_5}(t)$','$u(t)$','$y_{PDMTM alfaro123C}(t)$','$y_{POMTM vitec}(t)$','$y_{PDMTM Viteck}(t)$','Interpreter','latex','location','northeast')
xlabel ' Tiempo (s) ';
ylabel ' amplitud (%)';
% y_lim_sup = 100;
% y_lim_inf = -100;
% ylim([y_lim_inf, y_lim_sup])
% x_lim_min = -10;
% x_lim_max = 500;
% xlim([x_lim_min, x_lim_max])
grid on;

saveas(gcf,'imagenes/comparativa3Modelos','png')
saveas(gcf,'imagenes/comparativa3Modelos','epsc')
%--------------------------------------------------------------------
%--------------------------------------------------------------------
disp("Comparaci�n de �ndice de error cuadr�tico para los modelos POMTM y PDMTM obtenidos con m�todos de 2 puntos:")

% Integrales/sumas de error para POMTM Viteckov�:
e = y - y_POMTM_vitec;     % error
abs_error = abs(e);         % valor absoluto del error
cuadrado_error = e.^2;      % error al cuadrado 


%IAE = sum(abs_error)
ISE = sum(cuadrado_error);
disp("      >>> �ndice de error cuadr�tico para POMTM Viteckov�:        "+ ISE)

% Integrales/sumas de error para PDMTM Alfaro 123C:
e = y - y_PDMTM_alfaro123C;     % error
abs_error = abs(e);         % valor absoluto del error
cuadrado_error = e.^2;      % error al cuadrado 

%IAE = sum(abs_error)
ISE = sum(cuadrado_error);
disp("      >>> �ndice de error cuadr�tico para PDMTM Alfaro:           " + ISE)

% Integrales/sumas de error para PDMTM Viteckov�:
e = y - y_PDMTM_Vitec;     % error
abs_error = abs(e);         % valor absoluto del error
cuadrado_error = e.^2;      % error al cuadrado 

%IAE = sum(abs_error)
ISE = sum(cuadrado_error);
disp("      >>> �ndice de error cuadr�tico para PDMTM Viteckov�:        " + ISE)


%--------------------------------------------------------------------
%--------------------------------------------------------------------
% Gr�fica con modelos escogidos:
figure(3)
plot(t,y, t ,u,t,y_PDMTM_Vitec,'linewidth',2);
title 'Respuesta del sistema ante escal�n. Pruebas experimentales.';
legend('$y_{P_5}(t)$','$u(t)$','$y_{PDMTM Viteck}(t)$','Interpreter','latex','location','northeast')
xlabel ' Tiempo (s) ';
ylabel ' amplitud (%)';
% y_lim_sup = 100;
% y_lim_inf = -100;
% ylim([y_lim_inf, y_lim_sup])
% x_lim_min = -10;
% x_lim_max = 500;
% xlim([x_lim_min, x_lim_max])
grid on;

saveas(gcf,'imagenes/modeloEscogido','png')
saveas(gcf,'imagenes/modeloEscogido','epsc')

% %% funcion de lazo cerrado la cual se utiliza para verificar los metodos 
% kp=1.46;
% L2=-(exp(-30*s)*(1+0.4*exp(-45*s)))/(40*s+1);
% t=0:0.0001:4000;
% Myr=feedback(L2,1);
% u=heaviside(t);
% y=lsim(Myr,u,t);
% figure(4);
% plot(t,u,'b',t,y,'r')%%Entrada azul, salida roja
