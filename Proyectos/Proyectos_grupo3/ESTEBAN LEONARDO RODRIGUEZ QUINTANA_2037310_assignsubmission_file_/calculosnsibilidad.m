s = tf('s'); 
k_modelo_ = -1.4;
T_ = 33.67;
L_ = 15.42;
PDMTM_Vitec = (k_modelo_*exp(-L_*s))/(T_*s+1)^2;

%controlador ideal
CITAE = -1.288*(1+1/(99.07*s));
A = (PDMTM_Vitec)/(1 + PDMTM_Vitec*CITAE);

bode(A)
result = max(bode(A));
Ms = 10^(result/20);
disp("El valor es: "+Ms)

