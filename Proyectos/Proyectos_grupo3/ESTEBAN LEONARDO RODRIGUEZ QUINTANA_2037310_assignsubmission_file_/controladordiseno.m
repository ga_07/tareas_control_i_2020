% clc 
% clear
s=tf('s');
 
%% Para identificación de modelo de orden reducido de la planta P5:
clc
% funcion de transferencia de lazo abierto:
P5 = -(exp(-30*s)*(1+0.4*exp(-45*s)))/(40*s+1);  % planta asignada
t = 0:0.0001:2000;
u = heaviside(t-40)*50;
%u = ((0.1*heaviside(t-0.001)-0.4*heaviside(t-80)+0.7*heaviside(t-120)))*50;
u = transpose(u);
y = (lsim(P5,u,t))*.50+65;

% Constantes del modelo:
k_modelo_ = -1.4;
T_ = 33.67;
L_ = 15.42;

PDMTM_Vitec = (k_modelo_*exp(-L_*s))/(T_*s+1)^2;
% figure(1)
% saveas(gcf,'imagenes/anillosIAE','png')
% saveas(gcf,'imagenes/anillosIAE','epsc')
% fc_fragility_rings(1.67,86.85,0,PDMTM_Vitec);
% figure(2)
% saveas(gcf,'imagenes/anillosITAE','png')
% saveas(gcf,'imagenes/anillosITAE','epsc')
% fc_fragility_rings(1.288,99.07,0,PDMTM_Vitec);
% figure(3)
% fc_fragility_rings(1.506585,31.74,0,PDMTM_Vitec);
% saveas(gcf,'imagenes/anillosLopez','epsc')
% saveas(gcf,'imagenes/anillosLopez','png')

%% Para controladores diseñados y provados:

CIAE = -1.67*(1+1/(86.85*s));
Myd1 = PDMTM_Vitec/(1+CIAE*PDMTM_Vitec);

CITAE = -1.288*(1+1/(99.07*s));
Myd2 = PDMTM_Vitec/(1+CITAE*PDMTM_Vitec);

CIAE_MINIMIZADO_LOPEZ_PI = -1.506585*(1+1/(31.74*s));
Myd3 = PDMTM_Vitec/(1+CIAE_MINIMIZADO_LOPEZ_PI*PDMTM_Vitec);

y_CIAE = (lsim(Myd1,u,t))*.50+65;      % respuesta temporal del CIAE en lazo cerrado ante escalón
y_CITAE = (lsim(Myd2,u,t))*.50+65;      % respuesta temporal del CITAE en lazo cerrado ante escalón
y_CIAE_MINIMIZADO_LOPEZ_PI = (lsim(Myd3,u,t))*.50+65;      % respuesta temporal del CIAE_MINIMIZADO_LOPEZ_PI en lazo cerrado ante escalón

% Comparación de respuesta para controladores diseñados:
figure(4)
plot(t ,y, t ,u,t,y_CIAE,t,y_CITAE,t,y_CIAE_MINIMIZADO_LOPEZ_PI,'linewidth',1);
title 'Comparativa de Respuestas en Lazo Cerrado';
legend('$y_{P_5}(t)$','$u(t)$','$y_{IAE}(t)$','$y_{ITAE}(t)$','$y_{IAE MINIMIZADO LOPEZ PI}(t)$','Interpreter','latex','location','northeast')
xlabel ' Tiempo (s) ';
ylabel ' amplitud (%) ';
% y_lim_sup = .5;
% y_lim_inf = -.6;
% ylim([y_lim_inf, y_lim_sup])
% x_lim_min = -10;
% x_lim_max = 950;
% xlim([x_lim_min, x_lim_max])
grid on;

saveas(gcf,'imagenes/comparativaControladoresDiseñados_perturbacion_especifica','epsc')
saveas(gcf,'imagenes/comparativaControladoresDiseñados_perturbacion_especifica','png')

MydSistemaReal = P5/(1+CITAE*P5);

y_CITAE_real = (lsim(MydSistemaReal,u,t))*.50+65;      % Respuesta en LC de sistema real con controlador ITAE

%Comparativa respuesta real y del modelo para controlador ITAE diseñado y seleccionado:
figure(5)
plot(t ,u,t,y_CITAE,t,y_CITAE_real,'linewidth',1);
title 'Respuesta en LC para PI ITAE con sistema real y su modelo';
legend('$u(t)$','$y_{ITAE}(t)$','$y_{ITAE real}(t)$','Interpreter','latex','location','northeast')
xlabel ' Tiempo (s) ';
ylabel ' amplitud (%) ';
% y_lim_sup = .45;
% y_lim_inf = -.45;
% ylim([y_lim_inf, y_lim_sup])
% x_lim_min = -10;
% x_lim_max = 950;
% xlim([x_lim_min, x_lim_max])
grid on;

saveas(gcf,'imagenes/controladorDisenado','epsc')
saveas(gcf,'imagenes/controladorDisenado','png')
