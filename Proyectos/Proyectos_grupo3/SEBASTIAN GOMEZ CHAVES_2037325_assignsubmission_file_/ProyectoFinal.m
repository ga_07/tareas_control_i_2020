%% Sebastian Gomez & Marcelo Abad

%% Grafica del sistema real
hold on
plot(Real,'b');
plot(Escalon, 'r');
title('Respuesta del sistema "real"');
xlabel('Tiempo[s]');
ylabel('y(t)[%]');
legend('Respuesta del sistema real','Entrada Escalón')
grid on
hold off

%% Método de identificacion Alfaro
K=0.75;
yf=0.75;
t1=1.39;
t2=2.49;
yp=-0.329;
tp=0.31;
T=0.60*(t2-t1);
b=((1+(-yp/yf))/(exp(-tp/T)))-1;
a=((t1/T)-0.7538-(0.6262*b)+(0.0696*(b^2)))/(0.9275+(0.1794*b)-(0.0161*(b^2)));
display(b)
display(a)
display(T)
display(T*a)
display(T*-b)
s=tf('s');
P=K*(-b*T*s+1)/((T*s+1)*(a*T*s+1));
display(P)

%% Método de identificación Garcia-Gabin
clc
clear all

ti1=0.04;
ti2=0.12;
yss=0.75;
K=0.75;
ym=-0.328;
tm=0.31;
y2=0.4759;
t2=1.65;
tc=0.87;
t1=1.16;

taui=1.5*(ti2-ti1);
t00=ti2-taui;
tau=1.5*(t2-t1);
tau10=0.55*tau;
tau20=0.75*tau;
n0=0.30*((tau^2*ym)/(taui*yss));

garcia= @GarciaGabin; 
xo=[tau10 tau20 -n0 t00];
x = fsolve(garcia,xo)

%% Graficas de métodos de identificacion

hold on
plot(Real,'g','LineWidth',0.5);
plot(Escalon,':k');
plot(Alfaro,'--b');
plot(Gabin,'-.r');
title('Comparación de métodos de identifcación respecto a la respuesta "real"');
xlabel('Tiempo[s]');
ylabel('y(t)[%]');
legend('Respuesta del sistema real', 'Escalón', 'Alfaro','Garcia-Gabin')
hold off

%% Gráficas de métodos de identificación a lazo cerrado

hold on
plot(Real1,'g','LineWidth',0.5);
plot(Escalon,':k');
plot(Alfaro1,'--b');
plot(Gabin1,'-.r');
title('Comparación de métodos de identifcación respecto a la respuesta "real"');
xlabel('Tiempo[s]');
ylabel('y(t)[%]');
legend('Respuesta del sistema real', 'Escalón', 'Alfaro','Garcia-Gabin')
hold off

%% Sintonización PID Estandar
K=0.75;
T=0.5019;
T2=0.5020;
n=0.8453;
a=T2/T
b=n/T

a0=0.4659;
a1=0.8250;
a2=-0.2850;
a3=0.40860;

taucmin=a0+(a1*b)+(a2*b^2)+(a3*b^3)
tauc=0.05+(0.75*a)+(0.475*b)-(0.1875*a*b)

kaup=((-3*(tauc)^2*b)+((a+(1+a)*b)*(3*tauc+b))-(tauc)^3)/(tauc+b)^3
taui=((-3*(tauc)^2*b)+((a+(1+a)*b)*(3*tauc+b))-(tauc)^3)/(a+(1+a+b)*b)
taud=(((-b-1-a)*(tauc)^3)+(3*(tauc)^2*a)+(a*b*((3*tauc)+b)))/((-3*(tauc)^2*b)+((a+(1+a)*b)*(3*tauc+b))-(tauc)^3)

Kp=kaup/K
Ti=taui*T
Td=taud*T


%% Sintonizacion para PI2GdL Ms=1.4
T=0.5019;
T2=0.5020;
n=0.8453;
a=T2/T
b=n/T

a0=-2.486;
a1=2.765;
a2=-0.141;

b0=11.54;
b1=-2.237;
b2=6.451;
b3=-0.446;
b4=0.1767;
b5=1;
b6=0;

c0=0.4901;
c1=0.8543;
c2=-0.6064;
c3=0.5362;

Kc=a0+(a1*b^a2)
Ti=(b0+(b1*b))/(b2+(b3*b)+(b4*b^2)+(b5*b^3)+(b6*b^4))
B=c0+(c1*b)+(c2*b^2)+(c3*b^3)


%% Gráficas de sintonización de controladores

hold on
grid on
plot(Escalon1,'--r');
plot(Escalon2,'--b');
%plot(PI2GdL, 'b');
plot(PID_estandar,'b');
plot(Us1,'k');
title('Respuesta del sistema con controlador sintonizado ante cambios tipo escalon en la perturbación y valor deseado');
xlabel('Tiempo[s]');
ylabel('y(t)[%]');
legend('Referencia 85%', 'Perturbación 15%', 'PID Estandar','u(s)');
hold off

%% Grafica de salidas de controladores u(s)

hold on
grid on
plot(Escalon1,'--r');
plot(Escalon2,'--b');
plot(Us1,'b');
plot(Us2,'k');
title('Respuestas de las salida de los controladores ante cambios tipo escalon en la perturbación y valor deseado');
xlabel('Tiempo[s]');
ylabel('y(t)[%]');
legend('Referencia 85%', 'Perturbación 15%', 'PI2GdL','PID Estandar');
hold off

