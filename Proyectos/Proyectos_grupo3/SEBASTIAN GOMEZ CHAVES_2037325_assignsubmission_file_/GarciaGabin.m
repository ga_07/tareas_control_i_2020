function F = GarciaGabin(x)
     
K=0.75;
ym=-0.328;
tm=0.31;
y2=0.4759;
t2=1.65;
tc=0.87;
t1=1.16;

F(1)=K*(1 + ((((x(1) + x(2))/(x(3) - x(2))))*exp(-(tm - x(4))/(x(2)))) - ((((x(1) + x(3))/(x(3) - x(2))))*exp(-(tm - x(4))/(x(3))))) - ym;
F(2)=((-(x(1) + x(2))/(x(2)*(x(3) - x(2))))*exp(-(tm - x(4))/(x(2)))) + (((x(1) + x(3))/(x(3)*(x(3) - x(2))))*exp(-(tm - x(4))/(x(3))));
F(3)=K*(1 + ((((x(1) + x(2))/(x(3) - x(2))))*exp(-(tc - x(4))/(x(2)))) - ((((x(1) + x(3))/(x(3) - x(2))))*exp(-(tc - x(4))/(x(3)))));
F(4)=K*(1 + ((((x(1) + x(2))/(x(3) - x(2))))*exp(-(t2 - x(4))/(x(2)))) - ((((x(1) + x(3))/(x(3) - x(2))))*exp(-(t2 - x(4))/(x(3))))) - y2;
