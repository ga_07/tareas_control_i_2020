%% Sebastian Gomez & Marcelo Abad

s= tf('s');
%Planta identificada
P=(0.75*(-0.8544*s+1))/(0.2528*s^2+1.0057*s+1);

%Controladores
PI2 = 0.1107*(2.7704+(1/(0.3553*s)));
PID = 0.7003*(1+(1/(0.9570*s))+(0.2364*s));

%LA
L=P*PI2;
L1=P*PID;

%LC
PI2cerrado = 1/(1+L);
PIDcerrado = 1/(1+L1);

%Para PI
figure;
margin(L)

%Para PID
figure;
margin(L1)


%Margen de sensibilidad
Ms1=getPeakGain(PI2cerrado)
Ms2=getPeakGain(PIDcerrado)