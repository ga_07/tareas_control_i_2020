%% Simulaciones con prueba de lazo abierto - Comparacion con sistema real
P = tf(3.79,[40 1 0],'InputDelay',2); % Funcion de transferencia de la planta real
t = (0:0.5:20);
u = heaviside(t);
lsim(P,u,t);
hold on

P1 = tf(3.66,[1 0], 'InputDelay', 2.01); % Funcion de transferencia MIPOMTM
lsim(P1,u,t);
hold on

P2 = tf(3.66,[40.1 1 0], 'InputDelay', 2.01); %Funcion de transferencia MISOMTM
lsim(P2,u,t);
hold on

%% Simulaciones con ajuste por controlador P - Respuesta subamortiguada
P = tf(3.79,[40 1 0],'InputDelay',2); % Funcion de transferencia de la planta
t = (0:0.5:1000);
u = heaviside(t);
Kp=0.03;
LC=feedback(P, Kp);
lsim(LC,u,t);
%% Simulacion con ajuste por controlador P - Comparacion con sistema real
P = tf(3.79,[40 1 0],'InputDelay',2); % Funcion de transferencia de la planta real
t = (0:0.5:20);
u = heaviside(t-1);
lsim(P,u,t);
hold on

P3 = tf(5.71,[65.91 1 0],'InputDelay',2.16); % Funcion de transferencia para ajuste por controlador Kp
lsim(P3,u,t);
%% Simulacion para ajuste con base en la informacion critica - Respuesta oscilatoria
P = tf(3.79,[40 1 0],'InputDelay',2); % Funcion de transferencia de la planta
t = (0:0.5:200);
u = heaviside(t);
Kp=0.133; %Se determina que con Kp = 0.15 se obtiene una respuesta oscilatoria
LC=feedback(P, Kp);
lsim(LC,u,t);

%% Simulacion para ajuste con base en la informacion critica - Comparacion con sistema real
P = tf(3.79,[40 1 0],'InputDelay',2); % Funcion de transferencia de la planta
t = (0:0.5:20);
u = heaviside(t-1);
lsim(P,u,t);
hold on

P4 = tf(0.84,[1 0],'InputDelay',14.075); % Funcion de transferencia a partir de info critica - Primer orden
lsim(P4,u,t);
hold on

P5 = tf(5.71,[60.31,1 0],'InputDelay',1.05); % Funcion de transferencia para ajuste por controlador Kp
lsim(P5,u,t);
%% Simulacion todos los modelos obtenidos
P = tf(3.79,[40 1 0],'InputDelay',2); % Funcion de transferencia de la planta
t = (0:0.5:120);
u = heaviside(t);
lsim(P,u,t);
hold on

P4 = tf(0.84,[1 0],'InputDelay',14.075); % Funcion de transferencia a partir de info critica - Primer orden
lsim(P4,u,t);
hold on

P5 = tf(5.71,[60.31,1 0],'InputDelay',1.05); % Funcion de transferencia para ajuste por controlador Kp
lsim(P5,u,t);
hold on

P3 = tf(5.71,[65.91 1 0],'InputDelay',2.16); % Funcion de transferencia para ajuste por controlador Kp
lsim(P3,u,t);
hold on

P1 = tf(3.66,[1 0], 'InputDelay', 2.01); % Funcion de transferencia MIPOMTM
lsim(P1,u,t);
hold on

P2 = tf(3.66,[40.1 1 0], 'InputDelay', 2.01); %Funcion de transferencia MISOMTM
lsim(P2,u,t);
%% Indices IAE, ISE
clc
t = (0:0.5:20);
u = heaviside(t);
P = tf(3.79,[40 1 0],'InputDelay',2); % Funcion de transferencia de la planta

% Indice para P1
P1 = tf(3.66,[1 0], 'InputDelay', 2.01); % Funcion de transferencia MIPOMTM
e1 = lsim(P,u,t) - lsim(P1,u,t);
IAE1 = trapz(t, abs(e1));
disp(IAE1)
ISE1 = trapz(t, e1.^2);
disp(ISE1)

% Indice para P2
P2 = tf(3.66,[40.1 1 0], 'InputDelay', 2.01); %Funcion de transferencia MISOMTM
e2 = lsim(P,u,t) - lsim(P2,u,t);
IAE2 = trapz(t, abs(e2));
disp(IAE2)
ISE2 = trapz(t, e2.^2);
disp(ISE2)

% Indice para P3

P3 = tf(5.71,[65.91 1 0],'InputDelay',2.16); % Funcion de transferencia para ajuste por controlador Kp
e3 = lsim(P,u,t) - lsim(P3,u,t);
IAE3 = trapz(t, abs(e3));
disp(IAE3)
ISE3 = trapz(t, e3.^2);
disp(ISE3)

% Indice para P4

P4 = tf(0.84,[1 0],'InputDelay',14.075); % Funcion de transferencia a partir de info critica - Primer orden 
e4 = lsim(P,u,t) - lsim(P4,u,t);
IAE4 = trapz(t, abs(e4));
disp(IAE4)
ISE4 = trapz(t, e4.^2);
disp(ISE4)

% Indice para P5

P5 = tf(5.71,[60.31,1 0],'InputDelay',1.05); % Funcion de transferencia para ajuste por controlador Kp
e5 = lsim(P,u,t) - lsim(P5,u,t);
IAE5 = trapz(t, abs(e5));
disp(IAE5)
ISE5 = trapz(t, e5.^2);
disp(ISE5)
%% Sintonización del controlador
%Planta elegida P2
s = tf('s');
km=3.66;
tm=2.01;
Tm=40.1;

%Método de Wang
c1=0.0642 + 0.0047/s + (2*s)/(0.02*s+ 1);
P2Wang=feedback(P2,0.1);
margin(c1*P2Wang)
bode(1/(1+c1*P2Wang))
fc_fragility_rings(0.0642,0.0047/0.0642,2/0.0642,P2Wang)
%IAE
C=c1;
P=P2Wang;
L=C*P;
Ms=1/(1+C*P);
Myr=(C*P)/(1+(C*P));
Myd=(P)/(1+(C*P));

t = 0:0.01:250;     %Tiempo para graficar

r = ones(size(t));  %Valor deseado
r=r*0.5;            %Punto de operación
r(10000:end) = 0.6;

d = ones(size(t));  %Perturbación
d=d*0.5;
d(18000:end) = 0.55;

Y_r = lsim(Myr, r, t);
Y_d = lsim(Myd, d, t);

Y = Y_r + Y_d;
Ya=reshape(Y,[1 25001]);
e=abs(r-Ya);
IAE=e(10000:12200);
IAE=trapz(IAE)

% Método de Tan
c2=(1 +1/(45*s)+ 4.45*s)*(1/(1 + 0.2866*s));
margin(c2*P2)
bode(1/(1+c2*P2))
fc_fragility_rings(1,45,4.45,P2)
%IAE
C=c2;
P=P2;
L=C*P;
Ms=1/(1+C*P);
Myr=(C*P)/(1+(C*P));
Myd=(P)/(1+(C*P));

t = 0:0.01:250;     %Tiempo para graficar

r = ones(size(t));  %Valor deseado
r=r*0.5;            %Punto de operación
r(10000:end) = 0.6;

d = ones(size(t));  %Perturbación
d=d*0.5;
d(18000:end) = 0.55;

Y_r = lsim(Myr, r, t);
Y_d = lsim(Myd, d, t);

Y = Y_r + Y_d;
Ya=reshape(Y,[1 25001]);
e=abs(r-Ya);
IAE=e(10000:12200);
IAE=trapz(IAE)

% Método de liu et al.(2003)
c3=0.2794*(1+1/(50.6012*s)+8.7442*s);
margin(c3*P2)
bode(1/(1+c3*P2))
fc_fragility_rings(0.2794,50.6012,8.7442,P2)
%IAE
C=c3;
P=P2;
L=C*P;
Ms=1/(1+C*P);
Myr=(C*P)/(1+(C*P));
Myd=(P)/(1+(C*P));

t = 0:0.01:250;     %Tiempo para graficar

r = ones(size(t));  %Valor deseado
r=r*0.5;            %Punto de operación
r(10000:end) = 0.6;

d = ones(size(t));  %Perturbación
d=d*0.5;
d(18000:end) = 0.55;

Y_r = lsim(Myr, r, t);
Y_d = lsim(Myd, d, t);

Y = Y_r + Y_d;
Ya=reshape(Y,[1 25001]);
e=abs(r-Ya);
IAE=e(10000:12200);
IAE=trapz(IAE);
%% Comparacion de respuestas de modelo real vs modelos identificados - Uso de PID Autotune Matlab
% Este corre con el archivo comprespmodsPID.slx
clc

y = out.salidapreal;
plot(y);
hold on

y1 = out.salidap1;
plot(y1);
hold on

y2 = out.salidap2;
plot(y2);
hold on

y3 = out.salidap3;
plot(y3);
hold on

y4 = out.salidap4;
plot(y4);
hold on


y5 = out.salidap5;
plot(y5);
