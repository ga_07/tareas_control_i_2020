%Métodos de identificación
%Proyecto de sistemas de control
% Pamela Barquero Umaña Carné B60925
% Edin Yoxan Cascante Espinoza Carné B71724
% Alex Ramírez Binns Carné B76151
clc
clear 
close all
s = tf('s');
%Planta asignada (real)
P5 = (-exp(-30*s)*(1+0.4*exp(-45*s)))/(40*s+1);
figure(1)
step(P5)
legend()
y=step(P5);
%Método POMTM
%Broida
Kb = -1.4;
ab = 5.500;
bb = 2.800;
t1_28 = 50.5;
t2_40 = 63.5;
Tb = ab*(t2_40-t1_28);
Lb = bb*t1_28 + (1-bb)*t2_40;
Pbroida = (Kb*exp(-Lb*s))/(Tb*s+1);
figure(2)
step(Pbroida)
legend()
ybroida=step(Pbroida);
%Método de Ho et al
KHo=-1.4;
t_35= 57;
t_85= 125;
aHo=0.670;
bHo=1.290;
THo=aHo*(t_85-t_35);
LHo=(bHo*t_35)+((1-bHo)*t_85);
PHo= KHo*exp(-LHo*s)/(THo*s+1);
figure(3)
step(PHo)
legend()
yHo=step(PHo);

%Método PDMTM
%Método de Vitecková et al
Kv=-1.4;
t_33= 54.7;
t_70= 97.2;
av=0.794;
bv=1.937;
Tv=av*(t_70-t_33);
Lv=(bv*t_33)+((1-bv)*t_70);
Pv= Kv*exp(-Lv*s)/((Tv*s+1)*(Tv*s+1));
figure(4)
step(Pv)
legend()
yv=step(Pv);
%Método de Alfaro 123C
K = -1.4;
a = 0.578;
b = 1.555;
t1_25 = 47.2;
t2_75 = 104;
T = a*(t2_75-t1_25);
L = b*t1_25 + (1-b)*t2_75;
Palfaro2 = (K*exp(-L*s))/(T*s+1);
figure(5)
step(Palfaro2)
legend()
ya2=step(Palfaro2);
% Método SOMTM
% Alfaro 3 puntos
Ka=-1.4;
t_25=47.2;
t_50=76.4;
t_75=104;
Lprima=(1.5552*t_25)-(0.5552*t_75);
Lsegunda=(1.555*t_25)+((1-1.555)*t_75);
Tprima=0.5776*(t_75-t_25);
a=(t_50-Lsegunda-(1.4362*Tprima))/((1.9844*Tprima)-t_50+Lsegunda);
Tsegunda=(2*Tprima)/(1+a);
Palfaro3 = Ka*exp(-Lsegunda*s)/(((Tsegunda*s)+1)*((a*Tsegunda*s)+1));
figure(6)
step(Palfaro3)
legend()
figure(7)
step(P5,PHo,Pv, Palfaro3)
legend()
ya3=step(Palfaro3);
% Análisis mediante IAE

%POMTM
error1=y(1:139)-yHo(1:139);
absoluto1=abs(error1);
iae1=trapz(0.1,absoluto1);
disp(iae1)

%PDMTM
error2=y(1:139)-yv(1:139);
absoluto2=abs(error2);
iae2=trapz(0.1,absoluto2);
disp(iae2)

%SOMTM
error3=y(1:139)-ya3(1:139);
absoluto3=abs(error3);
iae3=trapz(0.1,absoluto3);
disp(iae3)