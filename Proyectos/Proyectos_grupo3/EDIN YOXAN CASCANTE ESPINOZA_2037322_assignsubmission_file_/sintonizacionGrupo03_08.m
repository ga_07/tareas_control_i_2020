% Proyecto de sistemas de control
% Pamela Barquero Umaña Carné B60925
% Edin Yoxan Cascante Espinoza Carné B71724
% Alex Ramírez Binns Carné B76151
clc
%% Plot de las respuestas del sistema y controlador
%Algunos de los datos como el out.Y son tomados del sGrupo03_08.slx, se
%necesita correr primero el controlador
figure(1)
subplot(2,1,1);
yp=out.y;
plot(yp)
grid on
title('Respuesta del sistema');
ylabel('Salida del sistema');
xlabel('tiempo (s)');
subplot(2,1,2);
cp=(out.c);
plot(cp)
grid on
title('Respuesta del controlador');
ylabel('Salida del controlador');
xlabel('tiempo (s)');
s=tf('s');
%% Indice IAE
y=out.y.data;
r=out.r.data;
iae = sum(abs(r - y))
%% Esfuerzo de control
cp1=out.c.data;
abs1=zeros(length(cp1),1);
v=length(cp1)-1;
for i=50:v
    abs1(i,1)=abs(cp1(i+1)-cp1(i));
end
TVud = sum(abs1)
%% Fragilidad y Robustez
figure(2)
p=-1*exp(-30*s)*((1/(40*s+1))+(0.4*exp(-45*s)/(40*s+1)));
fc_fragility_rings(0.8566,65.03,0,p)







