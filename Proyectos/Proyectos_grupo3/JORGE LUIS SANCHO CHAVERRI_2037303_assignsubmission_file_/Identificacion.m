%% Identificación

clear all
close all
clc

%Proceso real dado
s=tf('s');
P = tf([1.59],[28 1],'IODelay',61); %Proceso
t= 0:0.01:250; %Tiempo de simulación
u = 0.5*heaviside(t);
[y]=lsim(P, u, t);
figure(1)
plot(t,y,t,u)
title('Respuesta del proceso controlado real')
legend('y(t)','u(t)')
xlabel('Tiempo (s)')
ylabel('u(t), y(t)')

%% Metodo de Identifición 1: Dos puntos 
%Ganancia experimental
yf = 15.9;
yi = 0;
deltaY = yf-yi;
uf = 10;
ui = 0;
deltaU = uf-ui;
K = deltaY/deltaU;

%Constante de tiempo y retardo: Viteckova
%Valores de las constantes
a_viteckova = 1.245 ;
b_viteckova = 1.498 ;
%Valor en porcentaje de los punto de Viteckova
P1_viteckova = yf*0.33;
P2_viteckova = yf*0.70;
%Valor del tiempo en cada uno de los puntos cardinales de Viteckova
t1_viteckova = 72.17;
t2_viteckova = 94.76;
%Valor de la constante de tiempo
T_viteckova = a_viteckova*(t2_viteckova-t1_viteckova);
%Valor del retardo
L_viteckova = b_viteckova*t1_viteckova + (1-b_viteckova)*t2_viteckova;
%Modelo del Proceso
P_viteckova = tf([K],[T_viteckova 1],'IODelay',L_viteckova);
[y_viteckova]=lsim(P_viteckova, u, t);

%Constante de tiempo y retardo: Chen y Yang
%Valores de las constantes
a_chenyang = 1.4 ;
b_chenyang = 1.54 ;
%Valor en porcentaje de los punto de Chen y Yang
P1_chenyang = yf*0.33;
P2_chenyang = yf*0.67;
%Valor del tiempo en cada uno de los puntos cardinales de Chen y Yang
t1_chenyang = 72.14;
t2_chenyang = 91.95;
%Valor de la constante de tiempo
T_chenyang = a_chenyang*(t2_chenyang-t1_chenyang);
%Valor del retardo
L_chenyang = b_chenyang*t1_chenyang + (1-b_chenyang)*t2_chenyang;
%Modelo del Proceso
P_chenyang = tf([K],[T_chenyang 1],'IODelay',L_chenyang);
[y_chenyang]=lsim(P_chenyang, u, t);

%Constante de tiempo y retardo: Ho
%Valores de las constantes
a_Ho = 0.67 ;
b_Ho = 1.29 ;
%Valor en porcentaje de los punto de Ho
P1_Ho =yf*0.35;
P2_Ho =yf*0.85;
%Valor del tiempo en cada uno de los puntos cardinales de Ho
t1_Ho = 73.11;
t2_Ho = 113.9;
%Valor de la constante de tiempo
T_Ho = a_Ho*(t2_Ho-t1_Ho);
%Valor del retardo
L_Ho = b_Ho*t1_Ho + (1-b_Ho)*t2_Ho;
%Modelo del Proceso
P_Ho = tf([K],[T_Ho 1],'IODelay',L_Ho);
[y_Ho]=lsim(P_Ho, u, t);

%Constante de tiempo y retardo: Broida
%Valores de las constantes
a_broida = 5.5 ;
b_broida = 2.8 ;
%Valor en porcentaje de los punto de Broida
P1_broida =yf*0.28;
P2_broida =yf*0.40;
%Valor del tiempo en cada uno de los puntos cardinales de Broida
t1_broida = 70.17;
t2_broida = 75.23;
%Valor de la constante de tiempo
T_broida = a_broida*(t2_broida-t1_broida);
%Valor del retardo
L_broida = b_broida*t1_broida + (1-b_broida)*t2_broida;
%Modelo del Proceso
P_broida = tf([K],[T_broida 1],'IODelay',L_broida);
[y_broida]=lsim(P_broida, u, t);

%Constante de tiempo y retardo: Smith
%Valores de las constantes
a_smith = 1.5 ;
b_smith = 1.5 ;
%Valor en porcentaje de los punto de Smith
P1_smith =yf*0.28;
P2_smith =yf*0.63;
%Valor del tiempo en cada uno de los puntos cardinales de Smith
t1_smith = 70.17;
t2_smith = 89.11;
%Valor de la constante de tiempo
T_smith = a_smith*(t2_smith-t1_smith);
%Valor del retardo
L_smith = b_smith*t1_smith + (1-b_smith)*t2_smith;
%Modelo del Proceso
P_smith = tf([K],[T_smith 1],'IODelay',L_smith);
[y_smith]=lsim(P_smith, u, t);

%Constante de tiempo y retardo: Alfaro
%Valores de las constantes
a_alfaro = 0.910 ;
b_alfaro = 1.262 ;
%Valor en porcentaje de los punto de Alfaro
P1_alfaro =yf*0.25;
P2_alfaro =yf*0.75;
%Valor del tiempo en cada uno de los puntos cardinales de Alfaro
t1_alfaro = 69.1;
t2_alfaro = 99.76;
%Valor de la constante de tiempo
T_alfaro = a_alfaro*(t2_alfaro-t1_alfaro);
%Valor del retardo
L_alfaro = b_alfaro*t1_alfaro + (1-b_alfaro)*t2_alfaro;
%Modelo del Proceso
P_alfaro = tf([K],[T_alfaro 1],'IODelay',L_alfaro);
[y_alfaro]=lsim(P_alfaro, u, t);

%Comparación entre modelos
figure(2)
plot(t,y,t,u,t,y_viteckova,t,y_chenyang,t,y_Ho,t,y_broida,t,y_smith,t,y_alfaro)
title('Comparación entre el proceso real y los modelos')
legend('y(t)_{Planta}','u(t)','y(t)_{Viteckova}','y(t)_{Chen y Yang}','y(t)_{Ho}','y(t)_{Broida}','y(t)_{Smith}','y(t)_{Alfaro}')
xlabel('Tiempo (s)')
ylabel('Magnitud')

%Indice del error absoluto y cuadrático
e_viteckova = y - y_viteckova; %error
IAE_viteckova = sum(abs(e_viteckova)); %indice del error absoluto
ISE_viteckova = sum(e_viteckova.^2); %indice del error cuadratico
e_chenyang = y - y_chenyang; 
IAE_chenyang = sum(abs(e_chenyang)); 
ISE_chenyang = sum(e_chenyang.^2); 
e_Ho = y - y_Ho; 
IAE_Ho = sum(abs(e_Ho)); 
ISE_Ho = sum(e_Ho.^2); 
e_broida = y - y_broida; 
IAE_broida = sum(abs(e_broida));
ISE_broida = sum(e_broida.^2);
e_smith = y - y_smith; 
IAE_smith = sum(abs(e_smith)); 
ISE_smith = sum(e_smith.^2); 
e_alfaro = y - y_alfaro; 
IAE_alfaro = sum(abs(e_alfaro));
ISE_alfaro = sum(e_alfaro.^2);

%Alfaro da el menor IAE e ISE

%% Metodo de Identifición 2: Oscilación mantenida


t1= 0:0.01:1000;
u1 = 0.5*heaviside(t1);

% Pruebas para encontrar el Kp de oscilacion mantenida
%No es necesario para el calculo por lo que se comentan
% for Kp = 0:0.1:1
%     LC = feedback(Kp*P, 1);
%     [y1]=lsim(LC, u1, t1);
%     figure(3)
%     plot(t1,y1,t1,u1)
%     legend(sprintfc('Kp = %f',Kp))
%     hold on
% end
% 
% title('Pruebas variando Kp')
% xlabel('Tiempo (s)')
% ylabel('u(t), y(t) (%)')
% 
% hold off

% Valor de Kp para oscilacion mantenida
Kcu = 0.91984;
LC = feedback(Kcu*P, 1);

[y2]=lsim(LC, u1, t1);
figure(4)
plot(t1,y2,t1,u1)
title('Respuesta a Lazo Cerrado con C(s) = 0.91984')
legend('y(t)','u(t)')
xlabel('Tiempo (s)')
ylabel('Magnitud')

% Entre mas iteraciones de tiempo, mejor da Tu, esto no se grafica
t1= 0:0.01:100000;
u1 = 50*heaviside(t1);
[y2]=lsim(LC, u1, t1);

% Encuentra el periodo de oscilación, ocupa Signal Processing Toolbox
[~,peaklocs] = findpeaks(y2, t1);
Tu = mean(diff(peaklocs))

% MÉTODO CHEN

K = 12.0637/(0.2*(50 - 12.0637)); % Calculado con la salida de LC con Kc = 0.2
T = Tu/(2*pi)*sqrt(Kcu.^2*K.^2-1);
L = Tu/(2*pi)*(pi - atan(2*pi*T/Tu));

P_chen = tf([K],[T 1],'IODelay',L) %Proceso Chen

% MÉTODO 1D2U

T_prima = Tu/(2*pi)*sqrt(Kcu*K-1);
L_prima = Tu/(2*pi)*(pi - 2*atan(2*pi*T_prima/Tu));

T = 1.5758*T_prima;
L = L_prima + 0.5077*T_prima;

P_1D2u = tf([K],[T 1],'IODelay',L) %Proceso 1D2U

% Respuesta
[y_chen]=lsim(P_chen, u, t);
[y_1D2u]=lsim(P_1D2u, u, t);

%Comparación entre modelos
figure(5)
plot(t,y,t,u,t,y_chen,t,y_1D2u)
title('Comparación entre el proceso real y los modelos')
legend('y(t)','u(t)','y(t)_{Chen}','y(t)_{1D2u}')
xlabel('Tiempo (s)')
ylabel('Magnitud')

%Indice del error absoluto y error cuadratico
e_chen = y - y_chen; %error
IAE_chen = sum(abs(e_chen)); %indice del error absoluto
ISE_chen = sum(e_chen.^2); %indice del error cuadratico
e_1D2u = y - y_1D2u;
IAE_1D2u = sum(abs(e_1D2u));
ISE_1D2u = sum(e_1D2u.^2);

%Chen da el menor IAE e ISE








