%% Sintonización

%Con modelo de Chen (a lazo cerrado)
clear all
close all
clc

K = 1.59;
T = 28.1369;
L =  60.1850;
P = tf([K],[T 1],'IODelay',L);
tau = L/T;


%Método Khan and Lehman (1996), pág 48, PI
Kc = ((0.808/L)+(0.511/T)-(0.255/(sqrt(T*L))))*(T/K);
Ti=L*((0.808*T+0.511*L-0.255*sqrt(T*L))/(0.095*T+0.846*L-0.381*sqrt(T*L)));
s=tf('s');
C = Kc*(1+1/(Ti*s));
%No muy bueno

% %Edgad (1997), pag 38, PI % PEOR IAE E ISE
% K2 = 0.4/K;
% Ti2 = 0.5*L;
% C2 = (K2)*(1+1/(Ti2*s));

% Maibeth y Orestes
a1 = -0.1777 ; a2 = 0.3740 ; a3 = 0.0001 ;
b1 = -1.0425 ; b2 = 1.4387 ; b3 = 0.2887 ;
c1 = 2.1745 ; c2 = 0.3626 ;
Tao_maibeth = L / (L + T);

K2 = (a1*Tao_maibeth + a2) / (K*(Tao_maibeth + a3)) ;
Ti2 = (L*(b1*Tao_maibeth + b2)) / (Tao_maibeth + b3);
C2 = (K2)*(1+1/(Ti2*s));

% %Marlin (1995), pag 38, PI % SEGUNDO PEOR IAE E ISE
% K3 = (0.45/K);
% Ti3=0.54*L;
% C3 = K3*(1+1/(Ti3*s));

%ECOSEE Team (1996a), PI
K3 = 0.42/K;
Ti3 = 0.60*(L+T);
C3 = K3*(1+1/(Ti3*s));

% Nomura et Al.
K4 = 0.73/K*(0.3610 + 1/tau);
Ti4 = T + 0.3610*L;
Td4 = (0.0690*L + 0.3610*T)/(0.3610*L + T)*L;
C4 = K4*(1+(1/(s*Ti4))+s*Td4);

%Syrcos and Kookos(2005), pág 90, PID
K5 = (1/K)*(0.31+0.6*(T/L));
Ti5 = T*(0.777 + 0.45*(L/T));
Td5 = T*(0.44-0.56*(T/L)^2.2);
C5 = K5*(1+(1/(s*Ti5))+s*Td5);

% %Wang(1995a), pág 91, PID
% %mínimo ISE
% x1A = 0.7645;
% x2A = 0.6032;
% %mínimo ISE
% x1S = 0.9155;
% x2S = 0.7524;
% 
% K6 = ((x1A + x2A/(L/T))*(T+0.5*L))/(K*(T+L));
% Ti6 = T + 0.5*L;
% Td6 = (0.5*T*L)/(T+0.5*L);
% C6 = K6*(1+(1/(s*Ti6))+s*Td6);

% Branica et al
K6 = 1/K*(0.338 + 0.623*1/tau);
Ti6 = 1.228*L*tau.^(-0.480);
Td6 = 0.231*L*tau.^(-0.120);
C6 = K6*(1+(1/(s*Ti6))+s*Td6);

%% Fragilidad


% Controlador 1
figure("Name", "Estabilidad relativa para el controlador C1")
fc_fragility_rings(Kc,Ti,0,P);

% Controlador 2
figure("Name", "Estabilidad relativa para el controlador C2")
fc_fragility_rings(K2,Ti2,0,P);


% Controlador 3
figure("Name", "Estabilidad relativa para el controlador C3")
fc_fragility_rings(K3,Ti3,0,P);

% Controlador 4
figure("Name", "Estabilidad relativa para el controlador C4")
fc_fragility_rings(K4,Ti4,Td4,P);

% Controlador 5
figure("Name", "Estabilidad relativa para el controlador C5")
fc_fragility_rings(K5,Ti5,Td5,P);

% Controlador 6
figure("Name", "Estabilidad relativa para el controlador C6")
fc_fragility_rings(K6,Ti6,Td6,P);

%% Simulación en Simulink

out = sim('LazoControl');

%% Comparación e Indices de error

%Comparación entre modelos
figure("Name", "Comparación entre controladores")
plot(out.tout, out.r, out.tout, out.d, out.tout, out.y1, out.tout, out.y2, out.tout, out.y4, out.tout, out.y6)
title('Comparación entre controladores')
legend('r(t)', 'd(t)', 'y_{Khan y Lehman}(t)' , 'y_{Maibeth y Orestes}(t)' , 'y_{Nomura et al.}(t)' , 'y_{Branica et al}(t)')
xlabel('Tiempo (s)')
ylabel('Magnitud')

%Indice del error absoluto
e_1 = out.r - out.y1; %error
IAE_1 = sum(abs(e_1)); %indice del error absoluto
ISE_1 = sum(e_1.^2); %indice del error cuadratico

e_2 = out.r - out.y2;
IAE_2 = sum(abs(e_2)); 
ISE_2 = sum(e_2.^2);

e_3 = out.r - out.y3;
IAE_3 = sum(abs(e_3)); 
ISE_3 = sum(e_3.^2);

e_4 = out.r - out.y4;
IAE_4 = sum(abs(e_4)); 
ISE_4 = sum(e_4.^2);

e_5 = out.r - out.y5;
IAE_5 = sum(abs(e_5)); 
ISE_5 = sum(e_5.^2);

e_6 = out.r - out.y6;
IAE_6 = sum(abs(e_6)); 
ISE_6 = sum(e_6.^2);

%% Esfuerzo de control
TVu1 = sum(abs(diff(out.u1)))
TVu2 = sum(abs(diff(out.u2)))
TVu3 = sum(abs(diff(out.u3)))
TVu4 = sum(abs(diff(out.u4)))
TVu5 = sum(abs(diff(out.u5)))
TVu6 = sum(abs(diff(out.u6)))


%% Desempeño de dinamica

S1 = stepinfo(out.y1, out.tout)
S2 = stepinfo(out.y2, out.tout)
S3 = stepinfo(out.y3, out.tout)
S4 = stepinfo(out.y4, out.tout)
S5 = stepinfo(out.y5, out.tout)
S6 = stepinfo(out.y6, out.tout)




