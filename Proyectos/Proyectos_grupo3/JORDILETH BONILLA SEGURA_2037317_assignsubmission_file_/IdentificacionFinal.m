close all
clear all
clc
%% Introducci�n del sistema real

s=tf('s');

P4=((3.79)*(exp(-2*s)))/(s*(40*s+1))

[y,t]= step(P4,0: 0.1: 18500);

figure(1);
plot(t,y,'b'); hold on 
r = [ones(185001,1)]'; r1 = ones(185001,1);
plot(t,r,'k')
grid on, title ('Respuesta del sistema real, ante una entrada escal�n')
axis([0 160 0 400])
ylabel('y(t),u(t)');
xlabel('Tiempo (min)');

%%%Acercamiento
axis([0 100 0 20])

%% Metodo Gr�fico de Alfaro Para Sistemas Integrantes (Sin Publicar)

%%%%Curva tangente

h = mean(diff(t));
dy = gradient(y,h);
[M,I] = max(dy);

b = y(I)-M*t(I); 

yt=M*t+b;

plot(t,yt,'g')
legend('Salida','Entrada','RectaTangente');
ylabel('y(t),u(t),yt(t)');

T = (-b/M)-2;

hold off

%% Modelos integrantes de primer y segundo orden

MI1 =(M*(exp(-2*s)))/(s)
[yI1,tI1] = step(MI1,0: 0.1: 18500);
figure(2);
plot(tI1,yI1,'b');
grid on
title ('Modelo Integrante de primer orden mas tiempo muerto');
ylabel('yI1(t)');
xlabel('Tiempo (min)');

MI2 =(M*(exp(-2*s)))/(s*(T*s+1))
[yI2,tI2]= step(MI2,0: 0.1: 18500);
figure(3);
plot(tI2,yI2,'b');
grid on
title ('Modelo Integrante de segundo orden mas tiempo muerto(Gr�fico)');
ylabel('yI2(t)');
xlabel('Tiempo (min)');


%% Comparaci�n del sistema real con los modelos integrantes

figure(4);
plot(t,y,'b',tI1,yI1,'r-',tI2,yI2,'k-');
grid on
axis([0 200 0 900])
title ('Comparaci�n del sistema real con los modelos integrantes(Gr�ficos)');
legend('SistemaReal','MIP1','MIP2');
ylabel('y(t),yI1(t),yI2(t)');
xlabel('Tiempo (min)');

%% Metodo de Indentificaci�n a partir de la respuesta derivada del sistema

%%%%Nota: Se considera la eliminacion del integrador del sistema real 

Pder =((3.79)*(exp(-2*s)))/((40*s+1));

[yd,td]= step(Pder,0: 0.1: 18500);

figure(5);
plot(td,yd,'b');
grid on
axis([0 400 0 4])
title ('Derivada de la respuesta del sistema ante un escal�n');
ylabel('dy(t)');
xlabel('Tiempo (min)');

%%%Metodo de Alfaro de los dos puntos para sistemas de primer orden

%%%A partir de los valores obtenidos del gr�fico 

t75 = 57.5;

t25 = 13.5;

Td = 0.9102*(t75-t25)

t0 = 1.262*t25-0.262*t75

MI2D =(M*(exp(-t0*s)))/(s*(Td*s+1))
[yI2D,tI2D] = step(MI2D,0: 0.1: 18500);
figure(6);
plot(tI2D,yI2D,'b');
grid on
title ('Modelo Integrante de segundo orden mas tiempo muerto(M.D)');
ylabel('yI2D(t)');
xlabel('Tiempo (min)');


%% Indices de desempe�o para los modelos Integrantes de segundo orden

e1= abs(y-yI2);
e2= (y-yI2).^2;

IAEI2 = sum(e1)
ISEI2 = sum(e2)

e3= abs(y-yI2D);
e4= (y-yI2D).^2;

IAEI2D = sum(e3)
ISEI2D = sum(e4)

%% Estimaci�n por medio del toolbox de matlab 

%systemIdentification('EstimacionToolbox2')

%% Modelos de primer y segundo orden obtenidos del toolbox

ModeloP1=(3.74)/(s + 1.186e-13)
[yP1,tP1]=step(ModeloP1);
figure(7);
plot(tP1,yP1,'b');
grid on
title ('Modelo de Primer Orden');
ylabel('yP1(t)');
xlabel('Tiempo (min)');

ModeloS2=(0.09474)/(s^2 + 0.025*s + 2.257e-12)
[yS2,tS2]=step(ModeloS2);
figure(8);
plot(tS2,yS2,'b');
grid on
title ('Modelo de Segundo Orden Subamortiguado');
ylabel('yS2(t)');
xlabel('Tiempo (min)');

%% Comparaci�n del sistema real con los modelos de Primer y Segundo Orden

[yp,tp]= step(P4,0:10000000:10e13);

figure(9)
plot(tp,yp,'b',tS2,yS2,'k-')
grid on
axis([0 10e10 0 6e10])
title ('Comparaci�n del MS2 con el sistema real');
legend('SistemaReal','MS2');
ylabel('y(t),yS2(t)');
xlabel('Tiempo (min)');


figure(10)
plot(tp,yp,'b',tP1,yP1,'r-')
grid on
axis([0 4.7e13 0 3.5e13])
title ('Comparaci�n del MP1 con el sistema real');
legend('SistemaReal','MP1');
ylabel('y(t),yP1(t)');
xlabel('Tiempo (min)');