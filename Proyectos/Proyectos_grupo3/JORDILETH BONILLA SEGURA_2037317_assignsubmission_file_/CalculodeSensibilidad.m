close all
clear all
clc

s=tf('s');

P =((exp(-0.05*s)))/(s*(s+1));

%Sensibilidad de Chen/Seborg

figure(1)
fc_fragility_rings(0.58,7,0,P)

%Sensibilidad de Morert

figure(2)
fc_fragility_rings(0.51138,5.3718,0,P)

%Sensibilidad de IMC basado 2Dof

figure(3)
fc_fragility_rings(4.5396,2.297,0.56,P)