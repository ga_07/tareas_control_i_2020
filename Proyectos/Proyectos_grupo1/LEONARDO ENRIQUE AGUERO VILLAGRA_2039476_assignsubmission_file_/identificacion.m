%% Proyecto Sistemas de Control
%% Identificaci�n del modelo de una planta de tratamiento de agua
%% Leonardo Ag�ero Villagra [B70103]
%% Basado en la funci�n:
%% [1.59*e^(-61s)]/[28s+1]
%% Respuesta del proceso a una entrada escal�n unitario

clear;clc;close all;
s = tf('s');
T = 0:0.005:500;
P = (1.59*exp(-61*s))/(28*s+1);
U = heaviside(T);
Y = lsim(P,U,T);

plot(T,Y,'b-',T,U,'r-','LineWidth',0.7)
title('Respuesta del modelo del proceso a una entrada escal�n unitario')
xlabel('Tiempo [s]')
ylabel('Amplitud de la se�al')
legend('Respuesta del modelo P(s)', 'Entrada u(s)')

%% Modelo de identificaci�n 1: M�todo de 2 puntos (Ho, et al)

% *** Se va usar este para sintonizaci�n ***

p2 = (1.59*exp(-61.1584*s))/(27.4968*s+1);
y2 = lsim(p2,U,T);

plot(T,Y,'b-',T,y2,'m-',T,U,'k-','LineWidth',0.7)
title('Respuesta del modelo del proceso a una entrada escal�n unitario')
xlabel('Tiempo [s]')
ylabel('Amplitud de la se�al')
legend('Modelo caja negra del proceso','Respuesta del modelo (Ho, et al)', 'Entrada escal�n unitario')

%% Modelo de identificaci�n 2: informaci�n cr�tica

p3 = (1.5899*exp(-60.9955*s))/(27.9945*s+1);
y3 = lsim(p3,U,T);

plot(T,Y,'b-',T,y3,'m-',T,U,'k-','LineWidth',0.7)
title('Respuesta del modelo del proceso a una entrada escal�n unitario')
xlabel('Tiempo [s]')
ylabel('Amplitud de la se�al')
legend('Modelo caja negra del proceso','Respuesta del modelo (informaci�n cr�tica)', 'Entrada escal�n unitario')

%% IAE, ETC...

iaeH = sum(abs(Y-y2))
iseH = sum((Y-y2).^2)

iaeC = sum(abs(Y-y3))
iseC = sum((Y-y3).^2)