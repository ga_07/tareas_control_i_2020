%%Universidad de Costa Rica
%Escuela de Ingeniería Eléctrica
%IE0431 Sistemas de Control
%Proyecto Final I-2020
%Katherine Badilla Carvajal B40765
%Mauricio Venegas Mora B27183
%Hernando Peralta Lehmann B35188

%%Planta real
clear
s = tf('s');
Pr = (0.7)/(s^2);
x = 0:100;
yr=step(Pr,x);
plot(yr)
hold on;
%Curva tangente a la respuesta de la planta real

y = (59.4)*x-2512.5;

plot(x,y,'r');
xlabel('Tiempo(s)')
ylabel('Magnitud')
title('Gráfica recta tangente a la respuesta del sistema real')
legend('Respuesta del sistema real','Recta tangente')

%Modelos obtenidos
Pi1 = (59.4*(exp(-42.1*s)))/s;%Integrante primer orden mas tiempo muerto
Pi2 = (59.4*(exp(-13.73*s)))/(s*(28.37*s+1));%Integrante segundo orden mas tiempo muerto
Pi3 = (59.4)/s;%Integrante primer orden sin tiempo muerto
Pi4 = (59.4)/(s*(28.37*s+1));%Integrante segundo orden sin tiempo muerto
%Comparación de los modelos con la planta real
%Gráfica
opt = stepDataOptions;
opt.StepAmplitude = 0.1;
yrn=step(Pr,x,opt);
y1=step(Pi1,x,opt);
y2=step(Pi2,x,opt);
y3=step(Pi3,x,opt);
y4=step(Pi4,x,opt);
xr=((yrn/100));
x1=((y1/100));
x2=((y2/100));
x3=((y3/100));
x4=((y4/100));
figure
plot(x,xr,x,x1,x,x2,x,x3,x,x4)
xlabel('Tiempo(s)')
ylabel('Magnitud')
title('Gráfica comparación de modelos')
legend('Pr','Pi1','Pi2','Pi3','Pi4')

figure
plot(x,xr,x,x2)
xlabel('Tiempo(s)')
ylabel('Magnitud')
title('Gráfica comparación del modelo real con el modelo escogido')
legend('Pr','Pi2')

%Integrales de error aboluto IAE
e1=xr-x1;
iae1=trapz(x,abs(e1));
n1='IAE1='
X1= [n1,num2str(iae1)];
disp(X1)

e2=xr-x2;
iae2=trapz(x,abs(e2));
n2='IAE2='
X2= [n2,num2str(iae2)];
disp(X2)

e3=xr-x3;
iae3=trapz(x,abs(e3));
n3='IAE3='
X3= [n3,num2str(iae3)];
disp(X3)

e4=xr-x4;
iae4=trapz(x,abs(e4));
n4='IAE4='
X4= [n4,num2str(iae4)];
disp(X4)





