%%Universidad de Costa Rica
%Escuela de Ingenier�a El�ctrica
%IE0431 Sistemas de Control
%Proyecto Final I-2020
%Katherine Badilla Carvajal B40765
%Mauricio Venegas Mora B27183
%Hernando Peralta Lehmann B35188
clear
s=tf('s');
%Vector de tiempos
t = 0:0.01:2000;

%Parámetros de la FT del modelo
K=59.4;
L=13.73;
T=28.37;
%FT del proceso real

Gp=0.7/s^2;

%M�todo 1 Skogestad
%M�todo 2 Velazquez
%M�todo 3 Velazquez

%FT controlador 1
gc1=0.0167*(1+(1/(13*s))+(13*s));

%FT controlador 2 
gc2=0.00386*(1+(1/(47*s))+(23*s));

%FT controlador 3
gc3=0.00191*(1+(1/(63*s))+(19*s));


%Control PID 1
Myd1=Gp/(1+gc1*Gp);
ypid1=step(Myd1,t);

%Control PID 2
Myd2=Gp/(1+gc2*Gp);
ypid2=step(Myd2,t);

%Control PID 3
Myd3=Gp/(1+gc3*Gp);
ypid=step(Myd3,t);



opt = stepDataOptions;
opt.StepAmplitude = 0.1;

yl1 = step(Myd1, t, opt);
yl2 = step(Myd2, t, opt);
y13 = step(Myd3, t, opt);

%Normalizacion

xn1=((yl1/100));
xn2=((yl2/100));
xn3=((y13/100));

%Graficos
subplot(1,1,1)
plot(t,xn1,t,xn2,t,xn3)
%T�tulos y otras leyendas
title('Respuesta al escal�n - Regulador')
xlabel('tiempo'),ylabel('y(t)')
legend('PID M1','PID M2','PID M3')


%Ms y RF20
%Metodo 1
figure
fc_fragility_rings(0.0167,13,13, Gp)
%Metodo 2
figure
fc_fragility_rings(0.00386,47,23, Gp)
%Metodo 3
figure
fc_fragility_rings(0.00191,63,19, Gp)
