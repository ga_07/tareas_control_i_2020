s=tf("s")

%Respuesta escal�n del modelo real del sistema
P=(0.75*(1-0.85*s))/(0.5*s+1)^2
figure (1)
step(P*100);
hold on
%Respuesta escal�n de la aproximaci�n del modelo
P2=(0.75*(-0.89*s+1))/((0.66*s+1)*(0.48*s+1));
step(P2*100)


legend('Modelo real de la planta','Aproximaci�n de orden reducido','FontSize', 22)
xlabel('t(s)','FontSize', 22)
ylabel('y(s),r(s)%','FontSize', 22)



%u es la se�al de control
%r es la referencia
%d es la perturbacion
%y1 es la salida del controlador PID con tc=1.2
%y2 es la salida del controlador PID con tc=1.4
%y3 es la salida del controlador IMC-PID con derivador en el error
%y4 es la salida del controlador IMC-PID con derivador en la se�al
%realimentada





%Graficas de salida de las pruebas de controlador PID 
figure(4)
plot(out.r*100,'-.','LineWidth',2)
hold on
plot(out.d*100,'-','LineWidth',2)
plot(out.y1*100,'--','LineWidth',2)
plot(out.y2*100,'LineWidth',2)
legend('r(s)','d(s)','tc=1.2','tc=1.4','FontSize', 22)
xlabel('t(s)','FontSize', 22)
ylabel('y(s),r(s),d(s)%','FontSize', 22)
title('Respuesta del proceso a cambios en la referencia y perturbaci�n','FontSize', 22)



%Graficas de se�ales de control de las pruebas de controlador PID por metodo de Mendez
%y Rimolo
figure(5)
plot(out.r*100,'LineWidth',2)
hold on
plot(out.d*100,'-','LineWidth',2)
plot(out.u1*100,'-.','LineWidth',2)
plot(out.u2*100,'LineWidth',2)
xlabel('t(s)','FontSize', 22)
ylabel('u(s)%','FontSize', 22)
legend('r(s)','d(s)','tc=1.2','tc=1.4','FontSize', 22)
title('Respuesta de la se�al de control a cambios en la referencia y perturbaci�n','FontSize', 22)



%Graficas de salida de las pruebas de controlador IMC-PID
figure(6)

plot(out.r*100,'-.','LineWidth',2)
hold on
plot(out.d*100,'--','LineWidth',2)
plot(out.y3*100,'-.' ,'LineWidth',2)
plot(out.y4*100,'LineWidth',2)
legend('r(s)','d(s)','PID con derivada aplicada al error', 'PID con derivada en y(s)','FontSize', 22)
xlabel('t(s)','FontSize', 22)
ylabel('y(s),d(s),r(s)%','FontSize', 22)
title('Respuesta del modelo del proceso a cambios en la referencia y perturbaci�n','FontSize', 22)


%Graficas de se�al de control de las pruebas de controlador IMC-PID
figure(7)
plot(out.r*100,'LineWidth',2)
hold on
plot(out.d*100,'LineWidth',2)
plot(out.u3*100,'-.' ,'LineWidth',2)
plot(out.u4*100,'LineWidth',2)
xlabel('t(s)','FontSize', 22)
ylabel('u(s),d(s),r(s)%','FontSize', 22)
legend('r(s)','d(s)','u con el error derivado','u con derivada solo en y(s)','FontSize', 22)
title('Respuesta de la entrada del proceso a cambios en la referencia y perturbaci�n','FontSize', 22)




%Anillos de fragilidad para los controladores 

figure(2)
fc_fragility_rings(0.66,2.27,0.5,P2)
% 
figure(3)
fc_fragility_rings(0.4,1.74,0.15,P2)
%
figure(8)
fc_fragility_rings(1.01,1.14,0.2778,P2)
