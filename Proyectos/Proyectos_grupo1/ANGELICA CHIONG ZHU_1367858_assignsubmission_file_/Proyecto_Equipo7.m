clc
clear all
close all


%% CURVA DE REACCIÓN PARA LA IDENTEIFICACIÓN PARA LA MEDICIÓN DIRECTA

s=tf('s');
P = (1.59*exp(-61*s))/(28*s+1)
step(P)
title('Curva de reacción a una entrada escalón')


%% COPIAR LOS DATOS DE ENTRADA, TIEMPO Y SALIDA A UN EXCEL PARA DOS CAMBIOS 

s=tf('s');
P = (1.59*exp(-61*s))/(20*s+1)

t_esp=0:0.5:370;
t_esc=0:0.5:350;
t_noesc=0:0.5:350;

u_esc = heaviside(t_esc-20);
y_esc = step(P,t_esp-20);

u_noesc = 5*heaviside(t_noesc-20);
y_noesc = 5*step(P,t_esp-20);

figure(1)
plot(t_esc, u_esc, t_esc, y_esc)

figure(2)
plot(t_noesc, u_noesc, t_esc, y_noesc)

%% LEER LOS DATOS DE .TXT, Y ESTIMA UN MODELO

filename = 'escalonconretardo.txt';
delimiterIn = '\t';
headerlinesIn = 1;
A = importdata(filename,delimiterIn,headerlinesIn);
t_esc=(A.data(:, 1));
u_esc=(A.data(:, 2));
y_esc=(A.data(:, 3));
figure(1);
plot(t_esc,u_esc,t_esc,y_esc);
ylabel('Amplitud');
xlabel('Tiempo (s)');
title('Salida y entrada del sistema')

filename = 'noescalonconretardo.txt';
delimiterIn = '\t';
headerlinesIn = 1;
B = importdata(filename,delimiterIn,headerlinesIn);
t_noesc=(B.data(:, 1));
u_noesc=(B.data(:, 2));
y_noesc=(B.data(:, 3));
figure(1);
plot(t_noesc,u_noesc,t_noesc,y_noesc);
ylabel('Amplitud');
xlabel('Tiempo (s)');
title('Salida y entrada del sistema')

%Estimar
data1 = iddata(y_noesc,u_noesc,1) 

%Validar
data2 = iddata(y_esc,u_esc,1)

type1 = 'P1D';

sys1 = procest(data1,type1)

figure(5);
compare(data2,sys1)
ylabel('Amplitud');
xlabel('Tiempo ');
title('Estimación');

%% APLICAR UN ESCALÓN A LOS DOS MODELOS, CALCULA ISE E IAE

close all
clear all
clc

s=tf('s');
t=0:0.1:350;

P = (1.59*exp(-61*s))/(28*s+1)
Pm1 = (1.59*exp(-60.6*s))/(28.2*s+1)
Pm2 = (1.59*exp(-61.507*s))/(27.995*s+1)

yp = step (P,t)
ym1 = step (Pm1,t)
ym2 = step (Pm2,t)

plot (t,yp,t,ym1,t,ym2)

title('Salidas de modelos y planta para una entrada escalón')
xlabel('Tiempo')
ylabel('Amplitud')
legend('Planta','Medición directa', 'Toolbox')


IAE_Pm1 = trapz(t,abs(ym1-yp))
IAE_Pm2 = trapz(t,abs(ym2-yp))

ISE2_Pm1 = trapz(t,(ym1-yp).^2)
ISE2_Pm2 = trapz(t,(ym2-yp).^2)

%% Primero correr Simulink, y luego graficar salidas de controlador y sistema

r1 = out.r1;
d1 = out.d1;
u1 = out.u1;
u2 = out.u2;
y1 = out.y1;
y2 = out.y2;
t = out.tout;

figure(1)
hold on;
plot(t,r1,'--')
plot(t,d1,'--')
plot(t,y1)
plot(t,y2)
legend('Señal de referencia','Señal de perturbación','Salida del sistema PI','Salida del sistema PID')
ylabel('Amplitud (%)');
xlabel('Tiempo (s)');
title('Salida del sistema a cambios en la referencia y perturbación')
xlim([-2 1500])
ylim([-50 100])
hold off

figure(2)
hold on;
plot(t,r1,'--')
plot(t,d1,'--')
plot(t,u1)
plot(t,u2)
legend('Señal de referencia','Señal de perturbación','Salida del controlador PI','Salida del controlador PID')
ylabel('Amplitud (%)');
xlabel('Tiempo (s)');
title('Salida del controlador a cambios en la referencia y perturbación')
xlim([-2 1500])
ylim([-50 100])

%% Primero correr Simulink, y de ahí se calcula el IAE y el ISE.  
%El error máximo, tiempo de error máximo, y tiempo de asentamiento al error
%se encontró gráficamente en el Simulink
r1 = out.r1;
d1 = out.d1;
u1 = out.u1;
u2 = out.u2;
y1 = out.y1;
y2 = out.y2;
t = out.tout;

IAE_C1 = trapz(t,abs(r1-y1))
IAE_C2 = trapz(t,abs(r1-y2))

ISE2_C1 = trapz(t,(r1-y1).^2)
ISE2_C2 = trapz(t,(r1-y2).^2)

%% Sacar el Ms, y el A_m
clc 
clear all

s=tf('s');

P = (1.59*exp(-14.1*s))/(28.2*s+1)
C1 = (0.754717*(1+(1/22.56*s)))
C2 = 0.692*(1 + 0.0425*(1/s)+ 6.130*(0.5/(1+(0.5)/s)))
%bodemag(1/(1+(C1*P)))
%margin(C1*P)
bodemag(1/(1+(C2*P)))
%margin(C2*P)


%% Correr el Simulink para cambios en la entrada y sin perturbación, y 
%luego correrlo para cambios en la perturbación sin cambios en la entrada
r1 = out.r1;
d1 = out.d1;
u1 = out.u1;
u2 = out.u2;
y1 = out.y1;
y2 = out.y2;
t = out.tout;

TVur1 = 0;
TVur2 = 0;
%TVud1 = 0;
%TVud2 = 0;

for i = 1:1500
    TVur1 = TVur1 + abs(u1(i+1)-u1(i));
end

for i = 1:1500
    TVur2 = TVur2 + abs(u2(i+1)-u2(i));
end

%for i = 1:1500
%    TVud1 = TVud1 + abs(u1(i+1)-u1(i));
%end

%for i = 1:1500
%    TVud2 = TVud2 + abs(u2(i+1)-u2(i));
%end

TVur1
TVur2

%TVud1
%TVud2
