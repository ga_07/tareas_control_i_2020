%%Proyecto de control%%
%%Autores: Sebasti�n Rojas, Juli�n Benavides, Geovanny Bonilla.
%%Carnes respectivos: B66260, B30955, B61141.
%%Grupo 01
%%
%%Proceso Real%%%
s = tf('s');
e0=exp(-30*s);
e1=exp(-45*s);
Preal=-e0*(1+0.4*e1)/(40*s+1);
figure(1)
step(Preal)
%% Modelos de identifiaci�n:
%%m�todo de dos puntos PDMTM comparaci�n Ho vs Viteckov�:
e2=exp(-18.542*s);
e3=exp(-15.446*s);
%%%modelo 1
P1=-1.4*e2/(31.021*s+1)^2;
%%%modelo 2
P2=-1.4*e3/(33.348*s+1)^2;
%%comparaci�n de ambos modelos identificados con el real:
figure(2)
step(Preal,P1,P2)
%%
%%m�todo de dos puntos comparaci�n POMTM comparaci�n Ho vs Alfaro 123C:
e4=exp(-37.57*s);
e5=exp(-32.4446*s);
%%modelo 1
P3=-1.4*e4/(44.89*s+1);
%%modelo 2, modelo seleccionado
P4=-1.4*e5/(51.597*s+1);
figure(3)
%%comparaci�n de ambos modelos identificados con el real:
step(Preal,P3,P4)
%%%Comparaci�n del modelo seleccionado con el proceso real:
figure(4)
step(Preal,P4)
%%
%%Controladores y respuestas:
%%Controlador PI n�mero 1 para el modelo de POMTM (Alfaro 123):
%%parametros del controlador kp=5.68 con a=0.2, Ti=51.597.
kp=5.68;
Ti=51.597;
C1=kp*(1+1/Ti*s);
e5=exp(-32.4446*s);
P41=1.4*e5/(51.597*s+1);
Myd=P41/(1+P41*C1);
figure(5)
bode=(1/(1+C1*P41));
%%%Para obtener los anillos de fragilidad P4 debe ir con la ganancia
%%%positiva, por lo tanto, se cambia al signo a la hora de simular.
fc_fragility_rings(kp,Ti,0,P41)
%%
%%Controlador PI n�mero 2 para el modelo de POMTM (Alfaro 123C).
%%Par�metros kp=1.7039, ki=0.0330, Ti=1.7039/0.0330 param�tro de ajuste miu=1.5.
C2= kp+ki/s;
e5=exp(-32.4446*s);
P42=1.4*e5/(51.597*s+1);
figure(6)
%%Anillos de fragilidad obtenidos con el segundo controlador PI.
fc_fragility_rings(1.7039,1.7039/0.0330,0,P42)
















