%% Proyecto final de control
%Planta 5
%Kaseng Fong, Alejandro Zuñiga, Geovanni Diaz.  
%Profesora Mercedes 
%Grupo 08

%% Proceso Real de la planta 
s=tf('s');

P=(-exp(-30*s)*(1+0.4*exp(-45*s)))/(40*s+1)% Proceso 5

figure;
t=0:0.25:1000;  
ue=-heaviside(t-100);     
ye=lsim(P,ue,t);                
Ft = plot(t,ye,t,ue,'--') 
title('Curva de reacción del Proceso real ')
xlabel('tiempo (s)','FontSize', 12)
ylabel('Magnitud','FontSize', 12) 
legend('y(t)','u(t)');
set(Ft,'LineWidth',1.5);

%% Identificación del modelo 
%Método de Ho
P_ho=(-1.4*exp(-37.4*s))/(45.225*s+1)%nominal

ye1=lsim(P_min,ue,t);
ye2=lsim(P_ho,ue,t);
ye3=lsim(P_max,ue,t);
figure(2)
Ft2 = plot(t,ye,t,ue,'--',t,ye2) 
title('Curva de reacción, modelo de HO')
xlabel('tiempo (s)','FontSize', 12)
ylabel('Magnitud','FontSize', 12) 
legend('Real','u(t)','Metodo Ho');
set(Ft2,'LineWidth',1.5);


IAE = trapz(t,abs(ye-ye1));    
ISE = trapz(t,(ye-ye1).^2); 
fprintf("Valor del error por método Ho : \n")
fprintf("El valor del IAE es: %f \nEl valor del ISE es: %f",IAE,ISE)


%% Modelo P1D 

P1D =  (exp(-33.133*s)*(-1.4103))/((1+52.805*s ))            
y3=lsim(P1D,ue,t);

figure(4)
Ft2 = plot(t,ye,t,ue,'--',t,y3) 
title('Curva de reacción, modelo P1D')
xlabel('tiempo (s)','FontSize', 12)
ylabel('Magnitud','FontSize', 12) 
legend('y(t)','u(t)','P1D');
set(Ft2,'LineWidth',1.5);

IAE = trapz(t,abs(ye-y3));    
ISE = trapz(t,(ye-y3).^2); 
fprintf("Valor del error por modelo P1D : \n")
fprintf("El valor del IAE es: %f \nEl valor del ISE es: %f",IAE,ISE)
 
%% Metodo alfaro 

P_alfaro =(-1.4*exp(-32.39*s))/(51.779*s+1)% Proceso 5
y4=lsim(P_alfaro,ue,t);
figure(5)
Ft2 = plot(t,ye,t,ue,'--',t,y4) 
title('Curva de reacción, modelo Alfaro 123C')
xlabel('tiempo (s)','FontSize', 12)
ylabel('Magnitud','FontSize', 12) 
legend('y(t)','u(t)','Metodo 123C');
set(Ft2,'LineWidth',1.5);

IAE = trapz(t,abs(ye-y4));    
ISE = trapz(t,(ye-y4).^2); 
fprintf("Valor del error por método 123C : \n")
fprintf("El valor del IAE es: %f \nEl valor del ISE es: %f",IAE,ISE)

%% Metodo Broide 
P_broide =(-1.4*exp(-27.72*s))/(69.3*s+1)% Proceso 5
y5=lsim(P_broide,ue,t);
figure(6)
Ft2 = plot(t,ye,t,ue,'--',t,y5) 
title('Curva de reacción, modelo  Broide')
xlabel('tiempo (s)','FontSize', 12)
ylabel('Magnitud','FontSize', 12) 
legend('y(t)','u(t)','Metodo Broide');
set(Ft2,'LineWidth',1.5);

IAE = trapz(t,abs(ye-y5));    
ISE = trapz(t,(ye-y5).^2); 
fprintf("Valor del error por método Broide : \n")
fprintf("El valor del IAE es: %f \nEl valor del ISE es: %f",IAE,ISE)



%% Todas las aproxiamciones de los modelos escogidos 

figure(3)
ue=-heaviside(t-100);     
ye1=lsim(P_ho,ue,t);
ff = plot(t,ye,t,ue,'--',t,ye1,'-.',t,y3,'-.',t,y4,'-.',t,y5,'-.') 
title('Comparación de los modelos ')
xlabel('tiempo (s)','FontSize', 12)
ylabel('Magnitud','FontSize', 12) 
legend('Real','u(t)','Metodo Ho','Modelo P1D','Modelo 123C','Modelo Broide');
set(ff,'LineWidth',1.2);


