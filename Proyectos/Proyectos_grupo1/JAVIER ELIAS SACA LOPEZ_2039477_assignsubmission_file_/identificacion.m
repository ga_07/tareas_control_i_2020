clear
clc

s=tf('s');

%Modelo real

p=-2*s;

P = (3.79 * exp(p))/(s*(40*s + 1));

LC = (0.01*P)/(1+0.01*P)

[y,t] = step(LC);


%Metodo de Alfaro

p1=-2.73*s;

P_1 = (3.792 * exp(p1))/(s*(37.783*s + 1));

LC1 = (0.01*P_1)/(1+0.01*P_1)

[y1,t1] = step(LC1);

%Metodo 3 puntos

p2=-3.9*s;

P_2 = (3.3 * exp(p2))/(s*(36.5*s + 1));

LC2 = (0.01*P_2)/(1+0.01*P_2)

[y2,t2] = step(LC2);


%Metodo rele

p3=-2*s;

P_3 = (1.15 * exp(p3))/(s);

LC3 = (0.01*P_3)/(1+0.01*P_3)

[y3,t3] = step(LC3);



%graficado
plot(t,y,t1,y1,t2,y2,t3,y3)
grid on
xlabel('Tiempo [s]')
ylabel('Datos')
title('Comparación de métodos')
legend({'M. real','M. Alfaro','M. 3 parámetros','M. relé'},'Location','northeast')

%sisotool