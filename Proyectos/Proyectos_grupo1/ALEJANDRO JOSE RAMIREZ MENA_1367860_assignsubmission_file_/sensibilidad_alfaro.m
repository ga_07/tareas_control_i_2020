
%Bode para el controlador obtenido con el m�todo de alfaro

s=tf('s');
P = (0.75*(-0.85*s+1))/((0.5*s+1)^2);
P2= (0.7499*(-0.895*s+1))/((0.6589*s+1)*(0.365*s+1));
C = (0.2843*(1.093*s+1))/(1.093*s);
B1 = C*P;
B3=C*P2;
M1 = 1/(1+B1);
M3=1/(1+B3);
bodemag(M1,M3,{0.3,1000});
figure(1)
axis([0.100 1000 -1 4 ])
grid on


