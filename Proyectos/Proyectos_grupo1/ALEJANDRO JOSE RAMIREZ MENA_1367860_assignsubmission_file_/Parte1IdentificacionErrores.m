%% Proyecto Sistemas de Control. Grupo 1. Equipo de trabajo #1.

%% Análisis de planta

clear all ;
s=tf('s');
%Respuesta ante el escalon del proceso real
kp =0.75    
F155=(kp*(1-0.85*s))/((0.5*s+1)^2);
%grafica de modelo real
t=0:0.0001:40;
B1=F155;
u=1*heaviside(t-1);
figure(1)               %figura 1. Ejes, titulo, labels....
y=lsim(B1,u,t);
plot(t,u,'k',t,y,'r')
legend('Entrada escalon','Modelo real')
title ('Respuesta del proceso real')
xlabel('Tiempo (s)') 
ylabel('Magnitud')
axis([0 7 -0.38 1.05]);
grid on
%% Metodo de intendificacion #1 Alfaro 123

%Respuesta del metodo de alfaro
F22= (0.7499*(-0.895*s+1))/((0.6589*s+1)*(0.365*s+1))

%grafica de modelo de Alfaro
t=0:0.0001:40;
B2=F22;
u=1*heaviside(t-1);
figure(2)                   %figura 2. Ejes, titulo, labels....
x=lsim(B2,u,t);
plot(t,u,'k',t,x,'b')
legend('Entrada escalon',' Modelo Alfaro 123')
title ('Respuesta del proceso obtenido de Alfaro respuesta inversa')
xlabel('Tiempo (s)') 
ylabel('Magnitud')
axis([0 7 -0.38 1.05]);
grid on

%Comparacion de los dos modelos, el REAL EN F155, y el de ALFARO EN F22
t=0:0.0001:40;
B1=F155;
B2=F22;
u=1*heaviside(t-1);
figure(3)                   %figura 3. Ejes, titulo, labels....
y=lsim(B1,u,t);
x=lsim(B2,u,t);
plot(t,u,'k',t,y,'r',t,x,'b-.')
legend('Entrada escalon','Modelo real',' Alfaro 123')
title ('Respuesta de los dos procesos, Alfaro y real')
xlabel('Tiempo (s)') 
ylabel('Magnitud')
axis([0 7 -0.38 1.05]);
grid on
%% %% Metodo de intendificacion #2 Matlab Identification Toolbox

%Respuesta ante el escalon del proceso real esta en F155

tx = 0:0.001:8;    %creamos un array de tiempo de 8000s
yx = 1*heaviside(tx-1);   %aplicamos escalon de magntud 1, en el segundo 1

SalidaR = lsim(F155, yx, tx);  %creamos array de la salida real
Entrada= yx.';  %hacemos columna de entrada

%ya tenemos 2 columnas de entrada y salida, usamos la herramienta

%systemIdentification  

%exportamos la FT obtenida y la acomodamos, teniendo
FTool=(0.7499*(1-0.8496*s))/((0.5176*s+1)*(0.482*s+1));

%grafica de modelo de Matlab
t=0:0.0001:40;
B3=FTool;
u=1*heaviside(t-1);
figure(4)                   %figura 4. Ejes, titulo, labels....
x=lsim(B2,u,t);
plot(t,u,'k',t,x,'b')
legend('Entrada escalon',' Matlab Ident Toolbox')
title ('Respuesta del proceso obtenido de Matlab')
xlabel('Tiempo (s)') 
ylabel('Magnitud')
axis([0 7 -0.38 1.05]);
grid on

%Comparacion de los dos modelos, el REAL EN F155, y el de MATLAB EN FTOOL
t=0:0.0001:40;
B1=F155;
B2=FTool;
u=1*heaviside(t-1);
figure(5)                   %figura 5. Ejes, titulo, labels....
y=lsim(B1,u,t);
x=lsim(B2,u,t);
plot(t,u,'k',t,y,'r',t,x,'b-.')
legend('Entrada escalon','Modelo real',' Matlab Ident Toolbox')
title ('Respuesta de los dos procesos, Matlab y Real')
xlabel('Tiempo (s)') 
ylabel('Magnitud')
axis([0 7 -0.38 1.05]);
grid on

%% Errores
%Respuesta ante el escalon del proceso real esta en F155

%Respuesta del metodo de alfaro, la reescribimos
FA= (0.7499*(-0.895*s+1))/((0.6589*s+1)*(0.365*s+1));

%Respuesta del metodo de Matlab Identification Toolbox
FTool=(0.7499*(1-0.8496*s))/((0.5176*s+1)*(0.482*s+1));

%Vectores para el error
%%%
tx = 0:0.001:10;    %creamos un array de tiempo
yx = 1*heaviside(tx-1);   %aplicamos escalon de magntud 1, en el segundo 1
SalidaAlfaro = lsim(FA, yx, tx);  %creamos array de la salida de alfaro
SalidaMatlab = lsim(FTool, yx, tx);  %creamos array de la salida de Matlab Ident
SalidaRe = lsim(F155, yx, tx);  %creamos array de la salida real

%Graficamos, modelo Real vs Matlab vs Alfaro
figure(6);
plot(tx,SalidaAlfaro,'g',tx, SalidaMatlab,'b-',tx, SalidaRe,'r', tx, yx, 'k');  
legend('Alfaro','Matlab Ident', 'Real', 'Escalon');
xlabel('tiempo, [t]');      
ax = gca;                      %ejes, tamano de letra, leyendas, etc
ax.FontSize = 13;
ylabel('Magnitud'); 
axis([0 7 -0.38 1.05]);
grid on;
title('Comparación de los modelos obtenidos, ante entrada escalón');

%errores, ISE y IAE
EAlfaro = SalidaAlfaro - SalidaRe;    %diferencia de vectores real y Alfaro
EMatlab = SalidaMatlab - SalidaRe;    %diferencia de vectores real y Matlab

IAE1 = trapz(tx, abs(EAlfaro));   %error integral absoluto, Alfaro
ISE1 = trapz(tx, EAlfaro.^2);     %error integral cuadrado, ALfaro

IAE2 = trapz(tx, abs(EMatlab));   %error integral absoluto, Matlab
ISE2 = trapz(tx, EMatlab.^2);     %error integral cuadrado, Matlab

%IAE1
%ISE1
%IAE2
%ISE2

