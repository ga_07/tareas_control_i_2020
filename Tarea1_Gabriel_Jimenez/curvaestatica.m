clear;clc;
% Se define el dominio de las curvas
Qe = linspace(0,0.1,50);

% Se definen las constantes de las curvas
rho = 785;
g = 9.81;
Xvs = 0.4;
Kvs = 0.001;

% Curva est�tica con Xvs = 0.4
h = 1/(rho*g)*(Qe/(Xvs*Kvs)).^2;

% Se dibuja la curva
plot(Qe,h)

title('Curvas est�ticas del proceso')
xlabel('Caudal de entrada (m^{3}/s)')
ylabel('Nivel del tanque (m)')

% La altura m�xima del tanque es 3.6 m
ylim([0 3.6])

hold on

% Curva est�tica con Xvs = 0.5
Xvs = 0.5;
h = 1/(rho*g)*(Qe/(Xvs*Kvs)).^2;
plot(Qe,h)

% Curva est�tica con Xvs = 0.6
Xvs = 0.6;
h = 1/(rho*g)*(Qe/(Xvs*Kvs)).^2;
plot(Qe,h)

%Punto m�s probable
plot(69.37601351e-3,2.5,'o','MarkerSize',10);
x=69.37601351e-3*ones(50);
plot(x,h,'--k')
y=2.5*ones(50);
plot(Qe,y,'--k')


legend('X_{vs} = 0.4','X_{vs} = 0.5','X_{vs} = 0.6', 'Punto de operaci�n m�s probable', 'Location' , 'northwest');

hold off

% Imprime las curvas en el PATH actual en un PNG a 600 dpi.
print(gcf,'curvaestatica.png','-dpng','-r600');       