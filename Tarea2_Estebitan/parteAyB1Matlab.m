%% Soluci�n a) para caso 1:
% Determinaci�n de k_p para sistema estable mediante criterio de R-H
clc;
clear;
fprintf('############# \t SOLUCI�N PARA CASO 1 \t #############\n');
% Declaraciones �tiles:
s = tf('s');
m = 7;
a = max([6,7,7,6]);
b = min([6,7,7,6])+1;

% FT para controlador y proceso:
syms C1(s,k_p)
C1 = k_p*(1+1/s);
Ppso =0.1*m/((a*s+1)*(b*s+1));

fprintf('--------------------\tPARTE A)\t--------------------\n');

% Funci�n de transferencia de lazo cerrado en servocontrol: 
Myr = C1*Ppso/(1+C1*Ppso);
% Extraemos numerador y denominador de FTLC servo:
[N,L] = numden(Myr);
fprintf('El polinomio caracter�stico del sistema viene dado por:\n\t p_c(s) = %s\n\n',L)
fprintf('Note que, asumiento k_p > 0, todos los coeficientes est�n presentes y tienen el mismo \nsigno, por lo tanto, se cumple con las condiciones necesarios pero no suficientes \npara asegurar estabilidad.\n');

coeff = coeffs(L,s);

a3=coeff(4);
a2=coeff(3);
a1=coeff(2);
a0=coeff(1);


fprintf('\nUsamos el arreglo R-H para el polinomio caracter�stico para determinar estabilidad:\n');
fprintf('  s^3  |\t%s \t\t\t %s \t%s \t\t 0 \n','a3','a1','');
fprintf('  s^2  |\t%s \t\t\t %s \t%s \t\t 0 \n','a2','a0','');
fprintf('  s^1  |\t%s \t\t %s\t\t%s \t 0 \n','b11','b12','');
fprintf('  s^0  |\t%s \t\t %s \t\t%s \t 0 \n\n','b21','b22','');
fprintf('Entonces:\n');
fprintf('  s^3  |\t%s \t\t %s \t%s 0 \n',a3,a1,'');
fprintf('  s^2  |\t%s \t\t %s \t%s \t\t 0 \n',a2,a0,'');
fprintf('  s^1  |\t%s \t\t %s\t\t%s \t 0 \n','b11','b12','');
fprintf('  s^0  |\t%s \t\t %s \t\t%s \t 0 \n\n','b21','b22','');

b11 = (a2*a1-a3*a0)/(a2);
b12 = (a2*0-a3*0)/(a2);
b21 = (b11*a0-a2*b12)/(b11);
b22 = (b11*0-a2*0)/(b11);

fprintf('Entonces:\n');
fprintf('  s^3  |\t%s \t\t\t\t\t %s \t%s 0 \n',a3,a1,'');
fprintf('  s^2  |\t%s \t\t\t\t\t %s \t%s \t\t 0 \n',a2,a0,'');
fprintf('  s^1  |\t%s \t\t %s \t\t %s \t\t 0 \n',b11,b12','');
fprintf('  s^0  |\t%s \t\t\t\t\t %s \t\t %s  \t\t 0 \n\n',b21,b22,'');

fprintf('Para que el sistema sea estable debe de cumplirse que:\n')
fprintf('\t 1. \t %s > 0 \n', b11);
fprintf('\t 2. \t %s > 0 \n', b21);

fprintf('\nUtilizando el comando Reduce[{10 - (35*k_p)/2 > 0, 7*k_p > 0}, k_p]\nel software Mathematica, se obtiene que:\n');
fprintf('\t\t 0 < k_p < 4/7\n');

fprintf('\nSi k_p no se encuentra dentro del rango de estabilidad aparecer�an\n2 polos con parte real positiva en el plano complejo.\n\n\n');

%% Soluci�n b) para caso 1:
% Comprobaci�n de resultados obtenidos en a):

fprintf('--------------------\tPARTE B)\t--------------------\n*** Para un caso donde el valor de k_p est� dentro del rango de estabilidad:\n\t\t Tomando k_p = 1/2\n');
k_p = 1/2;
s = tf('s');

C1 = k_p*(1+1/s);
Ppso =0.1*m/((a*s+1)*(b*s+1));
fprintf('La FTLC servo es dada por:\n');
Myr = C1*Ppso/(1+C1*Ppso)
fprintf('Los polos de Myr ser�an:\n')
fprintf('\t%i\n',pole(Myr));
fprintf('\nPara k_p dentro del rango tenemos polos con parte real negativa.\nEsto confirma que el sistema es estable dentro del rango.\n');

fprintf('\n*** Para un caso donde el valor de k_p est� fuera del rango de estabilidad:\n\t\t Tomando k_p = 4/7+1\n');

k_p = 4/7+1;

C1 = k_p*(1+1/s);
Ppso =0.1*m/((a*s+1)*(b*s+1));
fprintf('La FTLC servo es dada por:\n');
Myr = C1*Ppso/(1+C1*Ppso)
fprintf('Los polos de Myr ser�an:\n')
fprintf('\t%i \n',pole(Myr));
fprintf('\nPara k_p = 1/2 tenemos 2 polos con parte real positiva,\nlo cual hace que el sistema se vuelva inestable\n');
