clc
%% Declaración de funciones de transferencia de plantas:
s = tf('s');
Ppsus = 0.01*7/((s^2+2*s+2)*(7*s+1));
Ppso =  0.01*7/((7*s+1)*(7*s+1));
Ppins =  0.01*7/((2*s-1)*(0.1*7*s+1)*(0.1*6*s+1));
Ppints =  0.01*7*(7*s+1)/(s*(0.1*6*s+1)*(0.1*7*s+1)*(0.1*6*s+1));
Ppii =  0.01*7*(6*s+1)^3/(s^2*(s-1)^2);

%% Soluciones para Ejercicio 1b:
% k baja:
fprintf("-----------------------------------------------------\n");
fprintf("Ejercicio 1b:\n");
k = 1;
fprintf("\nBaja, k = %d\n",k);
c = k*(1+1/s);
L = c*Ppso;
figure;
nyqlog(L)

% k alta:
k = 25;
fprintf("\nAlta, k = %d\n",k);
c = k*(1+1/s);
L = c*Ppso;
figure;
nyqlog(L);

%% Soluciones para Ejercicio 2b:
% k baja:
fprintf("-----------------------------------------------------\n");
fprintf("Ejercicio 2b:\n");
k = 1;
fprintf("\nBaja, k = %d\n",k);
c = k*(1+1/s);
L = c*Ppsus;
figure;
nyqlog(L);

% k alta:
k = 400;
fprintf("\nAlta, k = %d\n",k);
c = k*(1+1/s);
L = c*Ppsus;
figure;
nyqlog(L);

%% Soluciones para Ejercicio 3b:
fprintf("-----------------------------------------------------\n");
fprintf("Ejercicio 3b:\n");
% k baja:
k = 0.03;
fprintf("\nBaja, k = %d\n",k);
c = (k/s)*(s+1+i*(7+2))*(s+1-i*(7+2));
L = c*Ppsus;
figure;
nyqlog(L);

% k alta:
k = 1;
fprintf("\nAlta, k = %d\n",k);
c = (k/s)*(s+1+i*(7+2))*(s+1-i*(7+2));
L = c*Ppsus;
figure;
nyqlog(L);

%% Soluciones para Ejercicio 4b:
fprintf("-----------------------------------------------------\n");
fprintf("Ejercicio 4b:\n");
k = 10;
fprintf("\nBaja, k = %d\n",k);
c = k;
L = c*Ppins;
figure;
nyqlog(L);

% k media:
k = 25;
fprintf("\nMedia, k = %d\n",k);
c = k;
L = c*Ppins;
figure;
nyqlog(L);

% k alta:
k = 65;
fprintf("\nAlta, k = %d\n",k);
c = k;
L = c*Ppins;
figure;
nyqlog(L);

%% Soluciones para Ejercicio 5b:
fprintf("-----------------------------------------------------\n");
fprintf("Ejercicio 5b:\n");
% k baja:
k = 5;
fprintf("\nBaja, k = %d\n",k);
c = k;
L = c*Ppints;
figure;
nyqlog(L)

% k alta:
k = 35;
fprintf("\nAlta, k = %d\n",k);
c = k;
L = c*Ppints;
figure;
nyqlog(L);

%% Soluciones para Ejercicio 6b:
fprintf("-----------------------------------------------------\n");
fprintf("Ejercicio 6b:\n");
k=0.1;
fprintf("\nBaja, k = %d\n",k);
c = k;
L = c*Ppii;
figure;
nyqlog(L)

% k alta:
k=0.2;
fprintf("\nAlta, k = %d\n",k);
c = k;
L = c*Ppii;
figure;
nyqlog(L);
