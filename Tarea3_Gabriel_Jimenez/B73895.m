% Gabriel Jim�nez Amador - B73895 - Grupo 03

% NOTA: Correr el progama por secciones (Run Section), para que no despliegue toda
% la informaci�n de un solo.
% Claro, si quiere despliegar todo la informaci�n, adelante.

% Primero se corre esta secci�n y luego el que se quiera seg�n sea el caso.

clear;clc;

p = 3;
q = 8;
m = 9;
n = 5;

t = 0:0.0001:20;
u = n*heaviside(t - m);

s = tf('s');

%% Sistema L1

L1 = (s+p)*(s+q)*(s+m)/(s^3*(s-1));

figure('Name','LGR del sistema 1');
rlocus(L1)
datacursormode on

syms x

Kp = 2.536666667;
K = min([p q m n])*Kp;
LC1 = feedback(K*L1,1);
figure('Name','Respuesta del sistema 1 en el l�mite de estabilidad');
lsim(LC1, u, t)

Kp = 10000;
K = min([p q m n])*Kp;
LC1 = feedback(K*L1,1);
figure('Name','Respuesta del sistema 1 sin oscilaciones');
lsim(LC1, u, t)

Kp = 3;
K = min([p q m n])*Kp;
LC1 = feedback(K*L1,1);
figure('Name','Respuesta del sistema 1 subamortiguado');
lsim(LC1, u, t)


%% Sistema L2

L2 = (s+p)*(s+q)*(s+m)*(s+n)/((s^2 + 1.2*s + 12)*(s^2 + s + 7));

figure('Name','LGR del sistema 2');
rlocus(L2)
datacursormode on

syms x

Kp = 365;
K = min([p q m n])*Kp;
LC2 = feedback(K*L2,1);
figure('Name','Respuesta del sistema 2 sin oscilaciones');
lsim(LC2, u, t)

Kp = 0.1;
K = min([p q m n])*Kp;
LC2 = feedback(K*L2,1);
figure('Name','Respuesta del sistema 2 subamortiguado');
lsim(LC2, u, t)

%% Sistema L3

L3 = (s^2 + 2*s + 2)/((s-p)*(s+q)*(s+m));

figure('Name','LGR del sistema 3');
rlocus(L3)
datacursormode on

syms x

Kp = 36.66667;
K = min([p q m n])*Kp;
LC3 = feedback(K*L3,1);
figure('Name','Respuesta del sistema 3 en el l�mite de estabilidad');
lsim(LC3, u, t)

Kp = 70;
K = min([p q m n])*Kp;
LC3 = feedback(K*L3,1);
figure('Name','Respuesta del sistema 3 sin oscilaciones');
lsim(LC3, u, t)

Kp = 180;
K = min([p q m n])*Kp;
LC3 = feedback(K*L3,1);
figure('Name','Respuesta del sistema 3 subamortiguado');
lsim(LC3, u, t)

%% Sistema L4

L4 = 1/((s-p)*(s^2+q*s+7));

figure('Name','LGR del sistema 4');
rlocus(L4)
datacursormode on

syms x

Kp = 5;
K = min([p q m n])*Kp;
LC4 = feedback(K*L4,1);
figure('Name','Respuesta del sistema 4 con Kp = 5');
lsim(LC4, u, t)

Kp = 100;
K = min([p q m n])*Kp;
LC4 = feedback(K*L4,1);
figure('Name','Respuesta del sistema 4 con Kp = 100');
lsim(LC4, u, t)


