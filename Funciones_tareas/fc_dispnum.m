function fc_dispnum(txt1,num,txt2)
%V.M. Alfaro, 2004
%fc_dispnum despliega un n�mero (num) en la pantalla
%con texto antes (txt1) y despu�s (txt2)
%el texto posterior txt2 es optativo
%
%	fc_dispnum(txt1,num,txt2)
%
%		num   - n�mero a desplegar
%		txt1  - texto a desplegar antes del n�mero (string)
%		txt2  - texto a desplegar despu�s del n�mero (string)
%
if nargin < 3
  txt2='';
end
txtp=strcat(txt1,num2str(num),txt2);
disp(txtp)