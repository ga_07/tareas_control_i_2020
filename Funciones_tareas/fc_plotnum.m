function fc_plotnum(x,y,txt1,num,txt2)
%V.M. Alfaro, 2002
%fc_plot num despliega un n�mero (num) en la posici�n (x,y)
%de la figura gr�fica activa, con texto antes (txt1) y
%despu�s (txt2), el texto posterior txt2 es optativo
%
%	fc_plotnum(x,y,txt1,num,txt2)
%
%		x     - posici�n en el eje x (relativa a las escalas del gr�fico)
%		y     - posici�n en el eje y (relativa a las escalas del gr�fico)
%		num   - n�mero a desplegar
%		txt1  - texto a desplegar antes del n�mero (string)
%		txt2  - texto a desplegar despu�s del n�mero (string)
%
if nargin < 5
  txt2='';
end
txtp=strcat(txt1,num2str(num),txt2);
text(x,y,txtp)