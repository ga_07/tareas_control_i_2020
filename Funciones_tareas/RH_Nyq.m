%% 1
close all
s=tf('s');
C1=(s+1)/(s)
P1=0.02/((9*s+1)*(2*s+1)) %min +1 para no cancelar un polo con el cero del controlador
L1=minreal(C1*P1)
F1=feedback(L1,1)
polosLC=pole(F1)
%figHnd = figure;
figure; rlocus(L1)
disp('Como se aprecia en la Figura el rango de K es 0<K<100');
figure; nyqlog(L1) %Kbajas
figure; nyqlog(500*L1) %Kaltas
%% 2
s=tf('s');
C2=(s+1)/(s)
P2=0.02/((s^2+2*s+2)*(2*s+1)) %min +1 para no cancelar un polo con el cero del controlador
L2=minreal(C2*P2)
%figHnd = figure;
figure; rlocus(L2)
figure; nyqlog(L2) %Kbajas
figure; nyqlog(500*L2) %Kaltas
%% 3
s=tf('s');
C3=((s+1+11j)*(s+1-11j))/(s)
P3=0.02/((9*s+1)*(2*s+1)) %min +1 para no cancelar un polo con el cero del controlador
L3=minreal(C3*P3)
%figHnd = figure;
figure; rlocus(L3)
figure; nyqlog(0.1*L3) %Kbajas
figure; nyqlog(L3) %Kmedias
figure; nyqlog(500000*L3) %Kaltas
%% 4
s=tf('s');
C4=10
P4=0.02/((2*s-1)*(0.3*s+1)*(0.9*s+1)) %min +1 para no cancelar un polo con el cero del controlador
L4=minreal(C4*P4)
%figHnd = figure;
figure; rlocus(L4)
figure; nyqlog(L4) %Kbajas
figure; nyqlog(10*L4) %Kmedias
figure; nyqlog(100*L4) %Kaltas
%% 5
s=tf('s');
C5=10
P5=(0.01*(3*s+1))/(s*(0.3*s+1)*(0.9*s+1)*(0.4*s+1)) %min +1 para no cancelar un polo con el cero del controlador
L5=minreal(C5*P5)
%figHnd = figure;
figure; rlocus(L5)
figure; nyqlog(L5) %Kbajas
figure; nyqlog(1000*L5) %Kaltas
%% 6
s=tf('s');
C6=10
P6=(0.01*(1*s+1)^3)/(s^2*(s-1)^2) %min +1 para no cancelar un polo con el cero del controlador
L6=minreal(C6*P6)
%figHnd = figure;
figure; rlocus(L6)
figure; nyqlog(L6) %Kbajas
figure; nyqlog(1000*L6) %Kaltas
%% 7
s=tf('s');
C7=10
P7=(0.01*(4*s+1)*(s+1))/(s^2*(s-1)) %min +1 para no cancelar un polo con el cero del controlador
L7=minreal(C7*P7)
%figHnd = figure;
figure; rlocus(L7)
figure; nyqlog(L7) %Kbajas
figure; nyqlog(1000*L7) %Kaltas
%% 8
s=tf('s');
C8=10
P8=(0.01*(4*s+1)*(5*s+1))/(s^3) %min +1 para no cancelar un polo con el cero del controlador
L8=minreal(C8*P8)
%figHnd = figure;
figure; rlocus(L8)
figure; nyqlog(0.01*L8) %Kbajas
figure; nyqlog(10*L8) %Kaltas
%%
close all;
figure; nyqlog(L1)
figure; nyqlog(L2)
figure; nyqlog(L3)
figure; nyqlog(L4)
figure; nyqlog(L5)
figure; nyqlog(L6)
figure; nyqlog(L7)
figure; nyqlog(L8)