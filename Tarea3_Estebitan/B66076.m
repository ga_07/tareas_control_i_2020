% Esteban Rodr�guez Quintana, B66076, Tarea 3, Grupo 3
close all;
clear;
clc;


%% Declaraci�n de variables y sistemas:
% Miselaneas:
s = tf('s');
Carne = [6 6 6 7 6];
p = Carne(2);
q = Carne(3);
m = Carne(4);
n = Carne(5);

tfinal = 10;
paso = 0.01;
t = 0 : paso : tfinal;

kplanta = min(Carne);
u =  n*heaviside(t-m);

% Sistemas:
L1 = ((s+p)*(s+q)*(s+m))/((s^3)*(s-1));
L2 = ((s+p)*(s+q)*(s+m)*(s+n))/((s^2+1.2*s+12)*(s^2+s+7));
L3 = ((s^2+2*s+2))/((s-p)*(s+q)*(s+m));
L4 = (1)/((s-p)*(s^2+q*s+7));

% Revisiones con rlocus
figure;
rlocus(L1);
figure;
rlocus(L2);
figure;
rlocus(L3);
figure;
rlocus(L4);

%% sistema 1:
% Parte a, respuesta en el l�mite de estabilidad: 
%       --> K aprox 7.9
kp = 7.98/kplanta;
K = kplanta*kp;
[y,t] = lsim(feedback(K*L1,1),u,t);

figure;
plot(t,y,'b');
hold on;
plot(t,u,'--r');
hold on;
xlim([5 ,tfinal]);
legend('$L_1(s)$','$U(s)$','Interpreter','latex','location','northwest')
title 'Respuesta del sistema L1 en la frontera de estabilidad, K = 7.98';
xlabel ('$time (s)$','Interpreter','latex');
grid on;
saveas(gcf,'img/L1a','epsc');

% Parte b, respuesta sin oscilaciones:
%       --> A partir del LGR vemos que no hay respuesta que del todo 
%           no tenga oscilaciones.

% Parte c, respuesta subamortiguada:
%       --> 7.9 < K < inf
kp = 25/kplanta;
K = kplanta*kp;
[y,t] = lsim(feedback(K*L1,1),u,t);

figure;
plot(t,y,'b');
hold on;
plot(t,u,'--r');
hold on;
xlim([5 ,tfinal]);
legend('$L_1(s)$','$U(s)$','Interpreter','latex','location','northwest')
title 'Respuesta subamortiguada del sistema L1, K = 25';
xlabel ('$time (s)$','Interpreter','latex');
grid on;
saveas(gcf,'img/L1c','epsc');

%% Sistema 2:
% Parte a: respuesta del sistema en el l�mite de estabilidad:
%       --> Este sistema es estable para cualquier valor de kp


% Parte b, respuesta sin oscilaciones:
%       --> El sistema siempre tendr� oscilaciones, sin embargo se mostrar�
%           como se comporta ante ganancias muy altas

kp = 3*10^4/kplanta;
K = kplanta*kp;
[y,t] = lsim(feedback(K*L2,1),u,t);

figure;
plot(t,y,'b');
hold on;
plot(t,u,'--r');
hold on;
xlim([5 ,tfinal]);
legend('$L_2(s)$','$U(s)$','Interpreter','latex','location','northwest')
title 'Respuesta del sistema L2 parcialmente sin oscilaciones, K = 3x10^4';
xlabel ('$time (s)$','Interpreter','latex');
grid on;
saveas(gcf,'img/L2b','epsc');


% Parte c, respuesta subamortiguada:
%       --> 0 < K < inf
kp = 2.24/kplanta;
K = kplanta*kp;
[y,t] = lsim(feedback(K*L2,1),u,t);

figure;
plot(t,y,'b');
hold on;
plot(t,u,'--r');
hold on;
xlim([5 ,tfinal]);
legend('$L_2(s)$','$U(s)$','Interpreter','latex','location','northwest')
title 'Respuesta subamortiguada del sistema L2, K = 2.25';
xlabel ('$time (s)$','Interpreter','latex');
grid on;
saveas(gcf,'img/L2c','epsc');

%% Sistema 3:
% Parte a, respuesta en la frontera de la estabilidad: 
%       --> K aprox 126
tfinal = 15000;
paso = 0.005;
t = 0 : paso : tfinal;
kplanta = min(Carne);
u =  n*heaviside(t-m);

kp = 126.11/kplanta;
K = kplanta*kp;
[y,t] = lsim(feedback(K*L3,1),u,t);

figure;
plot(t,y,'b');
hold on;
plot(t,u,'--r');
hold on;
xlim([5 ,tfinal]);
legend('$L_3(s)$','$U(s)$','Interpreter','latex','location','northwest')
title 'Respuesta del sistema L3 en la frontera de la estabilidad, K = 126.5';
xlabel ('$time (s)$','Interpreter','latex');
grid on;
saveas(gcf,'img/L3a','epsc');

% Parte b, respuesta sin oscilaciones:
%       --> 129 < K < 213
tfinal = 30;
paso = 0.01;
t = 0 : paso : tfinal;
kplanta = min(Carne);
u =  n*heaviside(t-m);

kp = 200/kplanta;
K = kplanta*kp;
[y,t] = lsim(feedback(K*L3,1),u,t);

figure;
plot(t,y,'b');
hold on;
plot(t,u,'--r');
hold on;
xlim([5 ,tfinal]);
legend('$L_3(s)$','$U(s)$','Interpreter','latex','location','northwest')
title 'Respuesta del sistema L3 sin oscilaciones, K = 200';
xlabel ('$time (s)$','Interpreter','latex');
grid on;
saveas(gcf,'img/L3b','epsc');

% Parte c,respuesta subamortiguada:
%       --> K > 213
kp = 750/kplanta;
K = kplanta*kp;
[y,t] = lsim(feedback(K*L3,1),u,t);

figure;
plot(t,y,'b');
hold on;
plot(t,u,'--r');
hold on;
xlim([5 ,tfinal]);
legend('$L_3(s)$','$U(s)$','Interpreter','latex','location','northwest')
title 'Respuesta subamortiguada del sistema L3, K = 500';
xlabel ('$time (s)$','Interpreter','latex');
grid on;
saveas(gcf,'img/L3c','epsc');
%% Sistema 4:
%       --> Este sistema es inestable para cualquier valor de kp porque
%       siempre tiene una ra�z en el semiplano real positivo para cualquier
%       ganancia kp
%% Parte d: Validaci�n de a,b y c con la herramienta RLocusGui:
RLocusGui(L2);



