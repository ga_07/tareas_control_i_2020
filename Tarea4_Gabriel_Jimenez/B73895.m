% Gabriel Jim�nez Amador - B73895 - Grupo 03

%% Datos a importar del archivo de texto

clear;clc;

A = importdata("Prueba_exp.txt");
t=(A.data(:, 1));
u=(A.data(:, 2));
y=(A.data(:, 3)); 

Ts = 0.25;

%% Graficar entrada vs. salida

figure('Name','Entrada vs. salida');
plot(t, y, t, u, 'r--')
ylabel("Amplitud");
xlabel("Tiempo (s)");
title('Entrada vs. salida');
legend('y(t)', 'u(t)');
axis([566 3563 20 70]);
%% Restar el punto de operaci�n

u0 = 50;
y0 = 40.885636;
u1 = u- u0;
y1 = y - y0;

figure('Name','Entrada vs. salida sin el punto de operaci�n');
plot(t, y1, t, u1, 'r--')
ylabel("Amplitud");
xlabel("Tiempo (s)");
title('Entrada vs. salida sin el punto de operaci�n');
legend('y(t)', 'u(t)');
axis([566 3563 -20 30]);
yline(0,'k-', 'HandleVisibility','off');

%% Recortar datos para estimaci�n y validaci�n

u0 = 50;
y0 = 40.885636;
u1 = u - u0;
y1 = y - y0;
u1 = u - u0;

%L�mites
limiteEstimacion = 2567;
limiteValidacionA = 5847;
limiteValidacionB = 9911;

% Datos estimaci�n
tEstimacion = t(1:limiteEstimacion,1);
uEstimacion = u1(1:limiteEstimacion,1);
yEstimacion = y1(1:limiteEstimacion,1);

% Datos validaci�n
tValidacion = t(limiteValidacionA:limiteValidacionB,1);
uValidacion = u1(limiteValidacionA:limiteValidacionB,1);
yValidacion = y1(limiteValidacionA:limiteValidacionB,1);

figure('Name','Entrada vs. salida sin el punto de operaci�n');
plot(tEstimacion, yEstimacion, tEstimacion, uEstimacion, 'r--')
ylabel("Amplitud");
xlabel("Tiempo (s)");
title('Entrada vs. salida de datos a usar para estimaci�n');
legend('y(t)', 'u(t)');
yline(0,'k-', 'HandleVisibility','off');

figure('Name','Salida y entrada de los datos a usar para la validaci�n');
plot(tValidacion, yValidacion, tValidacion, uValidacion, 'r--')
ylabel("Amplitud");
xlabel("Tiempo (s)");
title('Entrada vs. salida de datos a usar para validaci�n');
legend('y(t)', 'u(t)');
yline(0,'k-', 'HandleVisibility','off');


%% Crear objeto con los datos para estimar y comparamos modelos

dataEstimacion = iddata(yEstimacion,uEstimacion,Ts);

dataValidacion = iddata(yValidacion,uValidacion,Ts);

type = 'P1D';
sys1 = procest(dataEstimacion,type)
figure('Name','Modelo 1');
compare(dataValidacion,sys1)
title('Comparaci�n con un modelo POMTM');
ylabel("Amplitud");
xlabel("Tiempo");

type = 'P2D';
sys2 = procest(dataEstimacion,type)
figure('Name','Modelo 2');
compare(dataValidacion,sys2)
title('Comparaci�n con un modelo SOMTM con polos reales');
ylabel("Amplitud");
xlabel("Tiempo");

type = 'P2DU';
sys3 = procest(dataEstimacion,type)
figure('Name','Modelo 3');
compare(dataValidacion,sys3)
title('Comparaci�n con un modelo SOMTM con polos complejos');
ylabel("Amplitud");
xlabel("Tiempo");

%% Comparacion real y estimada con punto de operaci�n

tModelo = 0:0.001:400;
uModelo = 5*heaviside(tModelo - 72) - 5*heaviside(tModelo - 416.75);
s = tf('s');
t0 = 566.50;
u0 = 50;
y0 = 40.885636;

yModelo = lsim(sys1, uModelo, tModelo);

tModelo = tModelo + 566.50;
yModelo = yModelo + y0;
uModelo = uModelo + u0;


yEstimacion = yEstimacion + y0;
uEstimacion = uEstimacion + u0;

figure('Name','Respuesta del sistema 1 en el l�mite de estabilidad');
plot(tModelo, yModelo, tModelo, uModelo, 'r--')
ylabel("Amplitud");
xlabel("Tiempo (s)");
title('Entrada vs. salida del modelo elegido y los datos');
hold on
plot(tEstimacion, yEstimacion, tEstimacion, uEstimacion, 'b--')
legend(texlabel('y_{modelo}(t)'), texlabel('u_{modelo}(t)'), texlabel('y_{estimacion}(t)'), texlabel('u_{estimacion}(t)'));
hold off



%% C�lculo de IAE e ISE

% Se obtienen los mismos tiempos tanto de tModelo como de tEstimacion
% Se obtienen los indices respectivos del arreglo tModelo
[sharedvals,idx] = intersect(tModelo,tEstimacion,'stable');

% m contiene la cantidad de tiempos iguales
[m, n] = size(idx);

% Se instancia el arreglo de errores
e = zeros(1, m);

% Se itera restando el valor correspondiente y del modelo como de la
% estimaci�n, seg�n los tiempos iguales encontrados
for i = 1:m
    error = yModelo(idx(i)) - yEstimacion(i);
    e(i) = error;
end

% Se obtienen los valores IAE e ISE
IAE = sum(abs(e))
ISE = sum(e.^2)

