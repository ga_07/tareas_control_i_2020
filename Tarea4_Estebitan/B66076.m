%% Tarea 4 identificacion sistemas 
%% Esteban Rodríguez Quintana. B66076. Grupo 3.
clc

%% Punto 1:
% Importación de datos experiemntales:
filename = 'prueba_exp.txt';
Muestras = importdata(filename);

t_original = Muestras.data(:,1);
u_original = Muestras.data(:,2);
y_original = Muestras.data(:,3);

figure(1)
plot(t_original ,y_original ,'b',t_original ,u_original ,'m-','linewidth',1);
title 'Entrada y salida del sistema, muestras experimentales.';
legend('$y(t)$','$u(t)$','Interpreter','latex','location','northwest')
xlabel ' Tiempo (s) ';
ylabel ' amplitud (%) ';
xlim([566.50,3562.75])
grid on;

saveas(gcf,'figuras/fig1y2','epsc')

%% Punto 2. Ajuste al punto de operación:
pto_op_u = 50;
pto_op_y = 40.885636;
y = y_original  - pto_op_y;
u = u_original  - pto_op_u;
t = t_original;

figure(2)
plot(t,y,'b',t,u,'m-','linewidth',1);
title 'Entrada y salida del sistema sin punto de operación.';
legend('$y(t)$','$u(t)$','Interpreter','latex','location','northwest')
xlabel ' Tiempo (s) ';
ylabel ' amplitud (%) ';
xlim([566.50,3562.75])
grid on;

saveas(gcf,'figuras/fig3a','epsc')


%% Punto 3b. Extracción de datos para estimación y validación del modelo:
t_estimacion = t(230:2000,1);
y_estimacion = y(230:2000,1);
u_estimacion = u(230:2000,1);

figure(3)
plot(t_estimacion,y_estimacion,'b',t_estimacion,u_estimacion,'m-','linewidth',1);
title 'Entrada y salida en rango para estimación del modelo.';
legend('$y(t)$','$u(t)$','Interpreter','latex','location','northwest')
xlabel ' Tiempo (s) ';
ylabel ' amplitud (%) ';
xlim([620,1050])
grid on;

saveas(gcf,'figuras/fig3b1','epsc')


t_validacion = t(2700:7000,1);
y_validacion = y(2700:7000,1);
u_validacion = u(2700:7000,1);

figure(4)
plot(t_validacion,y_validacion,'b',t_validacion,u_validacion,'m-','linewidth',1);
title 'Entrada y salida en rango para validación del modelo.';
legend('$y(t)$','$u(t)$','Interpreter','latex','location','northwest')
xlabel ' Tiempo (s) ';
ylabel ' amplitud (%) ';
xlim([1300,2300])
grid on;

saveas(gcf,'figuras/fig3b2','epsc')

%% Parte 4. Creación de objeto para estimar modelo:

data_id_estim = iddata(y_estimacion,u_estimacion,0.25);

%% Parte 5. Creación de objeto para validar modelo:

data_id_validacion = iddata(y_validacion,u_validacion,0.25);

%% Parte 6.1. Estimación del modelo 1:
type = 'P0';
M1 = procest(data_id_estim,type);
%% Parte 6.2. Estimación del modelo 2:
type = 'P1';
M2 = procest(data_id_estim,type);

%% Parte 6.3. Estimación del modelo 3:
type = 'P2';
M3 = procest(data_id_estim,type);

%% Parte 7.1. Validación del modelo 1 obtenido:
figure(5)
compare(data_id_validacion,M1);
title 'Validación de modelo para type = P0.';
xlabel ' Tiempo ';
ylabel ' amplitud (%) ';
xlim([230,1050])
grid on;

saveas(gcf,'figuras/fig7_1','epsc')
%% Parte 7.2. Validación del modelo 2 obtenido:
figure(6)
compare(data_id_validacion,M2);
title 'Validación de modelo para type = P1.';
xlabel ' Tiempo ';
ylabel ' amplitud (%) ';
xlim([230,1050])
grid on;

saveas(gcf,'figuras/fig7_2','epsc')
%% Parte 7.3. Validación del modelo 3 obtenido:
figure(7)
compare(data_id_validacion,M3);
title 'Validación de modelo para type = P2.';
xlabel ' Tiempo ';
ylabel ' amplitud (%) ';
xlim([230,1050])
grid on;

saveas(gcf,'figuras/fig7_3','epsc')

%% Parte 8 y 9 en reporte.
%% Parte 10.1 Validación del modelo obtenido:
t1 = t_original(230:2000,1);
u1 = 5*(heaviside(t1-638.25)-heaviside(t1-638.25-345));
y_mod = lsim(M2,u1,t1);

figure(8)
plot(t1,y_mod+pto_op_y,'b',t1,u1+pto_op_u,'b',t_estimacion,u_estimacion+pto_op_u,'m',t_estimacion,y_estimacion+pto_op_y,'g');
title 'Respuesta real y del modelo para validación del modelo en un rango dado.';
legend('$y_{real}(t)$','$y_{modelo}(t)$','$u(t)$','Interpreter','latex','location','northwest')
xlabel ' Tiempo (s) ';
ylabel ' amplitud (%) ';
xlim([624,1050])
grid on;

saveas(gcf,'figuras/fig10_1','epsc');

t1 = t_original;
u1 = u_original-pto_op_u;
y_mod = lsim(M2,u1,t1)+pto_op_y;


figure(9)
plot(t_original ,y_original ,'b',t_original,y_mod,'g',t_original ,u_original ,'m','linewidth',1);
title 'Respuesta real y del modelo para validación en toda la muestra.';
legend('$y_{real}(t)$','$y_{modelo}(t)$','$u(t)$','Interpreter','latex','location','northwest')
xlabel ' Tiempo (s) ';
ylabel ' amplitud (%) ';
xlim([566.50,3562.75])
grid on;

saveas(gcf,'figuras/fig10_2','epsc')

%% Parte 10.2 Validación con IAE e ISE:
e = y_original - y_mod;     % error
abs_error = abs(e);         % valor absoluto del error
cuadrado_error = e.^2;      % error al cuadrado 

% Integrales/sumas de error:
IAE = sum(abs_error)
ISE = sum(cuadrado_error)


