% Gabriel Jim�nez Amador - B73895 - Grupo 03

clear;clc;

p = 3;
q = 8;
m = 9;
n = 5;


a = max([p q m n]);
b = min([p q m n]) + 1;

s = tf('s');

P = (0.01*m)/((a*s + 1) * (b*s + 1));

%% Kp dentro del rango de estabilidad

fprintf('***********************************\nKp dentro del rango de estabilidad.\n***********************************\n')
Kp = 3;
C = Kp*(s+1)/s;

% Lazo realimentado
Myr = feedback(P*C,1);

fprintf('\nLos polos del sistema de control realimentado con Kp = 3:\n\n')
polos = pole(Myr)


figure('Name','LGR del sistema de control realimentado con Kp = 3');
rlocus(Myr)

fprintf('\nViendo los valores de los polos y el LGR del sistema, se observa que todos los polos\ndel sistema de control realimentado tienen parte real negativa.\n\n')


%% Kp fuera del rango de estabilidad

fprintf('***********************************\nKp fuera del rango de estabilidad.\n***********************************\n')
Kp = 20;
C = Kp*(s+1)/s;

% Lazo realimentado
Myr = feedback(P*C,1);

fprintf('\nLos polos del sistema de control realimentado con Kp = 20:\n\n')
polos = pole(Myr)


figure('Name','LGR del sistema de control realimentado con Kp = 20');
rlocus(Myr)

fprintf('\nViendo los valores de los polos y el LGR del sistema, se observa que hay dos polos\ncon parte real positiva en el sistema de control realimentado.\n\n')
