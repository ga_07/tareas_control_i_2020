% Gabriel Jim�nez Amador - B73895 - Grupo 03

% NOTA: Correr el progama por secciones (Run Section), para que no despliegue toda
% la informaci�n de los diagramas de Nyquist de un solo.
% Claro, si quiere despliegar todo la informaci�n, adelante.

% Primero se corre esta secci�n y luego el que se quiera seg�n sea el caso.

clear;clc;

p = 3;
q = 8;
m = 9;
n = 5;

a = max([p q m n]);
b = min([p q m n]) + 1;

s = tf('s');

%% Caso 1

Ps = (0.01*m)/((a*s + 1)*(b*s + 1));

% Ganancias bajas

Cs = 5 * (1 + 1/s);
Ls = Ps * Cs;

fprintf('\nCaso 1. Ganancias bajas:\n\n')
figure('Name','Diagrama de Nyquist para el caso 1 a ganancias bajas');
nyqlog(Ls)
xlabel('Population')

% Ganancias altas

Cs = 30 * (1 + 1/s);
Ls = Ps * Cs;

fprintf('\nCaso 1. Ganancias altas:\n\n')
figure('Name','Diagrama de Nyquist para el caso 1 a ganancias altas');
nyqlog(Ls)


%% Caso 2

Ps = (0.01*m)/((s^2 + 2*s + 2)*(b*s + 1));

% Ganancias bajas

Cs = 5 * (1 + 1/s);
Ls = Ps * Cs;

fprintf('\nCaso 2. Ganancias bajas:\n\n')
figure('Name','Diagrama de Nyquist para el caso 2 a ganancias bajas');
nyqlog(Ls)

% Ganancias altas

Cs = 100 * (1 + 1/s);
Ls = Ps * Cs;

fprintf('\nCaso 2. Ganancias altas:\n\n')
figure('Name','Diagrama de Nyquist para el caso 2 a ganancias altas');
nyqlog(Ls)

%% Caso 3

% Este caso tiene casos muy extremos de ganancia

Ps = (0.01*m)/((a*s + 1)*(b*s + 1));

% Ganancias bajas

Cs = 0.01/s*(s + 1 + 1i*(a + 2))*(s + 1 - 1i*(a + 2));
Ls = Ps * Cs;

fprintf('\nCaso 3. Ganancias bajas:\n\n')
figure('Name','Diagrama de Nyquist para el caso 3 a ganancias bajas');
nyqlog(Ls)

% Ganancias medias

Cs = 30/s*(s + 1 + 1i*(a + 2))*(s + 1 - 1i*(a + 2));
Ls = Ps * Cs;

fprintf('\nCaso 3. Ganancias medias:\n\n')
figure('Name','Diagrama de Nyquist para el caso 3 a ganancias medias');
nyqlog(Ls)

% Ganancias altas

Cs = 30000/s*(s + 1 + 1i*(a + 2))*(s + 1 - 1i*(a + 2));
Ls = Ps * Cs;

fprintf('\nCaso 3. Ganancias altas:\n\n')
figure('Name','Diagrama de Nyquist para el caso 3 a ganancias altas');
nyqlog(Ls)


%% Caso 4

a1 = 0.1 * max([p q m n]);
b1 = 0.1 * min([p q m n]);

Ps = (0.01*m)/((2*s - 1)*(a1*s + 1)*(b1*s + 1));

% Ganancias bajas

Cs = 10;
Ls = Ps * Cs;

fprintf('\nCaso 4. Ganancias bajas:\n\n')
figure('Name','Diagrama de Nyquist para el caso 4 a ganancias bajas');
nyqlog(Ls)

% Ganancias medias

Cs = 30;
Ls = Ps * Cs;

fprintf('\nCaso 4. Ganancias medias:\n\n')
figure('Name','Diagrama de Nyquist para el caso 4 a ganancias medias');
nyqlog(Ls)

% Ganancias altas

Cs = 80;
Ls = Ps * Cs;

fprintf('\nCaso 4. Ganancias altas:\n\n')
figure('Name','Diagrama de Nyquist para el caso 4 a ganancias altas');
nyqlog(Ls)


%% Caso 5

b2 = 0.1 * p;
g = 0.1*q;
d = 0.1*n;

Ps = ((0.01*m)*(a*s + 1))/(s*(b2*s + 1)*(g*s + 1)*(d*s + 1));

% Ganancias bajas

Cs = 5;
Ls = Ps * Cs;

fprintf('\nCaso 5. Ganancias bajas:\n\n')
figure('Name','Diagrama de Nyquist para el caso 5 a ganancias bajas');
nyqlog(Ls)


% Ganancias altas

Cs = 20;
Ls = Ps * Cs;

fprintf('\nCaso 5. Ganancias altas:\n\n')
figure('Name','Diagrama de Nyquist para el caso 5 a ganancias altas');
nyqlog(Ls)


%% Caso 6

b3 = min([p q m n]);
Ps = ((0.01*m)*(b3*s + 1)^3)/(s^2*(s - 1)^2);

% Ganancias bajas

Cs = 0.5;
Ls = Ps * Cs;

fprintf('\nCaso 6. Ganancias bajas:\n\n')
figure('Name','Diagrama de Nyquist para el caso 6 a ganancias bajas');
nyqlog(Ls)


% Ganancias altas

Cs = 20;
Ls = Ps * Cs;

fprintf('\nCaso 6. Ganancias altas:\n\n')
figure('Name','Diagrama de Nyquist para el caso 6 a ganancias altas');
nyqlog(Ls)
